-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2018-01-08 10:19:45.716

-- tables
-- Table: Branch
CREATE TABLE Branch (
    id varchar(50)  NOT NULL,
    title varchar(50)  NOT NULL,
    CONSTRAINT Branch_pk PRIMARY KEY (id)
);

-- Table: Groups
CREATE TABLE Groups (
    id varchar(50)  NOT NULL,
    Branch_id varchar(50)  NOT NULL,
    Plan_id varchar(50)  NOT NULL,
    title varchar(50)  NOT NULL,
    CONSTRAINT Groups_pk PRIMARY KEY (id)
);

-- Table: Plan
CREATE TABLE Plan (
    id varchar(50)  NOT NULL,
    Branch_id varchar(50)  NOT NULL,
    title varchar(50)  NOT NULL,
    CONSTRAINT Plan_pk PRIMARY KEY (id)
);

-- Table: Room
CREATE TABLE Room (
    room_num int  NOT NULL,
    Branch_id varchar(50)  NOT NULL,
    room_type int  NOT NULL,
    CONSTRAINT Room_pk PRIMARY KEY (room_num,Branch_id)
);

-- Table: Schedule
CREATE TABLE Schedule (
    id varchar(50)  NOT NULL,
    confirmed boolean  NOT NULL,
    Branch_id varchar(50)  NOT NULL,
    CONSTRAINT Schedule_pk PRIMARY KEY (id,confirmed)
);

-- Table: Study_class
CREATE TABLE Study_class (
    Schedule_id varchar(50)  NOT NULL,
    confirmed boolean  NOT NULL,
    Teacher_id varchar(50)  NOT NULL,
    Subject_id varchar(50)  NOT NULL,
    Group_id varchar(50)  NOT NULL,
    weekday int  NOT NULL,
    position int  NOT NULL,
    Room_room_num int  NOT NULL,
    Branch_id varchar(50)  NOT NULL,
    CONSTRAINT Study_class_pk PRIMARY KEY (Schedule_id,confirmed,Teacher_id,Subject_id,Group_id,weekday,position,Room_room_num)
);

-- Table: Subject
CREATE TABLE Subject (
    id varchar(50)  NOT NULL,
    Branch_id varchar(50)  NOT NULL,
    title varchar(50)  NOT NULL,
    lab_load int  NOT NULL,
    lec_load int  NOT NULL,
    sem_load int  NOT NULL,
    CONSTRAINT Subject_pk PRIMARY KEY (id)
);

-- Table: Subject_for_teacher
CREATE TABLE Subject_for_teacher (
    Subject_id varchar(50)  NOT NULL,
    Teacher_id varchar(50)  NOT NULL,
    CONSTRAINT Subject_for_teacher_pk PRIMARY KEY (Subject_id,Teacher_id)
);

-- Table: Subject_in_plan
CREATE TABLE Subject_in_plan (
    Subject_id varchar(50)  NOT NULL,
    Plan_id varchar(50)  NOT NULL,
    CONSTRAINT Subject_in_plan_pk PRIMARY KEY (Subject_id,Plan_id)
);

-- Table: Teacher
CREATE TABLE Teacher (
    id varchar(50)  NOT NULL,
    Branch_id varchar(50)  NOT NULL,
    surname varchar(50)  NOT NULL,
    name varchar(50)  NOT NULL,
    mid_name varchar(50)  NOT NULL,
    CONSTRAINT Teacher_pk PRIMARY KEY (id)
);

-- foreign keys
-- Reference: Group_Branch (table: Groups)
ALTER TABLE Groups ADD CONSTRAINT Group_Branch
    FOREIGN KEY (Branch_id)
    REFERENCES Branch (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Group_Plan (table: Groups)
ALTER TABLE Groups ADD CONSTRAINT Group_Plan
    FOREIGN KEY (Plan_id)
    REFERENCES Plan (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Plan_Branch (table: Plan)
ALTER TABLE Plan ADD CONSTRAINT Plan_Branch
    FOREIGN KEY (Branch_id)
    REFERENCES Branch (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Room_Branch (table: Room)
ALTER TABLE Room ADD CONSTRAINT Room_Branch
    FOREIGN KEY (Branch_id)
    REFERENCES Branch (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Schedule_Branch (table: Schedule)
ALTER TABLE Schedule ADD CONSTRAINT Schedule_Branch
    FOREIGN KEY (Branch_id)
    REFERENCES Branch (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Study_class_Group (table: Study_class)
ALTER TABLE Study_class ADD CONSTRAINT Study_class_Group
    FOREIGN KEY (Group_id)
    REFERENCES Groups (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Study_class_Room (table: Study_class)
ALTER TABLE Study_class ADD CONSTRAINT Study_class_Room
    FOREIGN KEY (Room_room_num, Branch_id)
    REFERENCES Room (room_num, Branch_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Study_class_Schedule (table: Study_class)
ALTER TABLE Study_class ADD CONSTRAINT Study_class_Schedule
    FOREIGN KEY (Schedule_id, confirmed)
    REFERENCES Schedule (id, confirmed)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Study_class_Subject (table: Study_class)
ALTER TABLE Study_class ADD CONSTRAINT Study_class_Subject
    FOREIGN KEY (Subject_id)
    REFERENCES Subject (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Study_class_Teacher (table: Study_class)
ALTER TABLE Study_class ADD CONSTRAINT Study_class_Teacher
    FOREIGN KEY (Teacher_id)
    REFERENCES Teacher (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Subject_Branch (table: Subject)
ALTER TABLE Subject ADD CONSTRAINT Subject_Branch
    FOREIGN KEY (Branch_id)
    REFERENCES Branch (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Subject_for_teacher_Subject (table: Subject_for_teacher)
ALTER TABLE Subject_for_teacher ADD CONSTRAINT Subject_for_teacher_Subject
    FOREIGN KEY (Subject_id)
    REFERENCES Subject (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Subject_for_teacher_Teacher (table: Subject_for_teacher)
ALTER TABLE Subject_for_teacher ADD CONSTRAINT Subject_for_teacher_Teacher
    FOREIGN KEY (Teacher_id)
    REFERENCES Teacher (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Subject_in_plan_Plan (table: Subject_in_plan)
ALTER TABLE Subject_in_plan ADD CONSTRAINT Subject_in_plan_Plan
    FOREIGN KEY (Plan_id)
    REFERENCES Plan (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Subject_in_plan_Subject (table: Subject_in_plan)
ALTER TABLE Subject_in_plan ADD CONSTRAINT Subject_in_plan_Subject
    FOREIGN KEY (Subject_id)
    REFERENCES Subject (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Teacher_Branch (table: Teacher)
ALTER TABLE Teacher ADD CONSTRAINT Teacher_Branch
    FOREIGN KEY (Branch_id)
    REFERENCES Branch (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- End of file.

