-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2018-01-08 10:19:45.716

-- foreign keys
ALTER TABLE Groups
    DROP CONSTRAINT Group_Branch;

ALTER TABLE Groups
    DROP CONSTRAINT Group_Plan;

ALTER TABLE Plan
    DROP CONSTRAINT Plan_Branch;

ALTER TABLE Room
    DROP CONSTRAINT Room_Branch;

ALTER TABLE Schedule
    DROP CONSTRAINT Schedule_Branch;

ALTER TABLE Study_class
    DROP CONSTRAINT Study_class_Group;

ALTER TABLE Study_class
    DROP CONSTRAINT Study_class_Room;

ALTER TABLE Study_class
    DROP CONSTRAINT Study_class_Schedule;

ALTER TABLE Study_class
    DROP CONSTRAINT Study_class_Subject;

ALTER TABLE Study_class
    DROP CONSTRAINT Study_class_Teacher;

ALTER TABLE Subject
    DROP CONSTRAINT Subject_Branch;

ALTER TABLE Subject_for_teacher
    DROP CONSTRAINT Subject_for_teacher_Subject;

ALTER TABLE Subject_for_teacher
    DROP CONSTRAINT Subject_for_teacher_Teacher;

ALTER TABLE Subject_in_plan
    DROP CONSTRAINT Subject_in_plan_Plan;

ALTER TABLE Subject_in_plan
    DROP CONSTRAINT Subject_in_plan_Subject;

ALTER TABLE Teacher
    DROP CONSTRAINT Teacher_Branch;

-- tables
DROP TABLE Branch;

DROP TABLE Groups;

DROP TABLE Plan;

DROP TABLE Room;

DROP TABLE Schedule;

DROP TABLE Study_class;

DROP TABLE Subject;

DROP TABLE Subject_for_teacher;

DROP TABLE Subject_in_plan;

DROP TABLE Teacher;

-- End of file.

