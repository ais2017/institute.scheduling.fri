from enum import IntEnum, unique


# type of work: lab/lecture/seminar
@unique
class Activity(IntEnum):
    LAB = 0
    LEC = 1
    SEM = 2


class Room(object):
    def __init__(self, room_num, room_type):
        self.room_num = int(room_num)  # use room_num as an id
        self.room_type = room_type

    @property
    def room_type(self):
        return self._room_type

    @room_type.setter
    def room_type(self, value):
        if not isinstance(value, Activity):
            raise Exception("room_type: invalid room type!")
        self._room_type = value
