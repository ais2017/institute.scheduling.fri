from scheduling.domain.plan import *


class Group(object):
    def __init__(self, id, title, plan):
        self.id = id
        self.title = title
        self.plan = plan

    @property
    def plan(self):
        return self._plan

    @plan.setter
    def plan(self, value):
        if not isinstance(value, Plan):
            raise Exception("plan: invalid plan!")
        else:
            self._plan = value

