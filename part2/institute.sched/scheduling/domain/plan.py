from scheduling.domain.subject import *


class Plan(object):
    def __init__(self, id, title, subjects):
        self.id = id
        self.title = title
        self.subjects = subjects

    @property
    def subjects(self):
        return self._subjects

    @subjects.setter
    def subjects(self, value):
        if not isinstance(value, list):
            raise Exception("subjects: not a list!")
        else:
            for i in value:
                if not isinstance(i, Subject):
                    raise Exception("subjects: not a subject list!")
        self._subjects = value

