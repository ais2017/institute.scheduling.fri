# Separate file to hold the shared variables

CLASSES_WEEKLY_LIMIT = 2  # week limit for type of classes
TEACHER_LOAD_LIMIT = 24  # how many classes in a week
DAILY_CLASSES_LIMIT = 5  # how many classes a day
SCHEDULES_NUM = 3  # how many times to create different schedules

