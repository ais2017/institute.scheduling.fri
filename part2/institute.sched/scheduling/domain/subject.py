from scheduling.domain.Global import *


class Subject(object):

    _lab_load = 0
    _lec_load = 0
    _sem_load = 0

    def __init__(self, id, title, lab_load, lec_load, sem_load):
        self.id = id
        self._title = title
        self.lab_load = lab_load
        self.lec_load = lec_load
        self.sem_load = sem_load

    @property
    def title(self):
        return self._title

    @property
    def lab_load(self):
        return self._lab_load

    @lab_load.setter
    def lab_load(self, value):
        if (value > CLASSES_WEEKLY_LIMIT) or (value < 0):
            raise Exception("lab_load: classes week limit exceeded!")
        self._lab_load = value

    @property
    def lec_load(self):
        return self._lec_load

    @lec_load.setter
    def lec_load(self, value):
        if (value > CLASSES_WEEKLY_LIMIT) or (value < 0):
            raise Exception("lec_load: classes week limit exceeded!")
        self._lec_load = value

    @property
    def sem_load(self):
        return self._sem_load

    @sem_load.setter
    def sem_load(self, value):
        if (value > CLASSES_WEEKLY_LIMIT) or (value < 0):
            raise Exception("sem_load: classes week limit exceeded!")
        self._sem_load = value
