from scheduling.domain.studyclass import *


class GroupSchedule(object):
    def __init__(self, group):
        self.group = group
        self.classes_list = []

    @property
    def classes_list(self):
        return self._classes_list

    @classes_list.setter
    def classes_list(self, value):
        if not isinstance(value, list):
            if not isinstance(value, StudyClass):
                raise Exception("classes_list: not a study class!")
        else:
            for i in value:
                if not isinstance(i, StudyClass):
                    raise Exception("classes_list: not a classes list!")
        self._classes_list = value

    def add_study_class(self, value):
        if not isinstance(value, StudyClass):
            raise Exception("classes_list: not a study class!")
        self._classes_list.append(value)

