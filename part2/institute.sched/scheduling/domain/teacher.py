from scheduling.domain.subject import *
from scheduling.domain.Global import *


class Teacher(object):
    def __init__(self, id, surname, name, mid_name, subjects):
        self.id = id
        self.name = name
        self.surname = surname
        self.mid_name = mid_name
        self.subjects = subjects  # list of the subjects
        self.full_load = TEACHER_LOAD_LIMIT # how many classes in a week

    @property
    def subjects(self):
        return self._subjects

    @subjects.setter
    def subjects(self, value):
        if not isinstance(value, list):
            raise Exception("subjects: not a list!")
        else:
            for i in value:
                if not isinstance(i, Subject):
                    raise Exception("subjects: not a subject list!")
        self._subjects = value

    @property
    def full_load(self):
        return self._full_load

    @full_load.setter
    def full_load(self, value):
        if (value > TEACHER_LOAD_LIMIT) or (value < 0):
            raise Exception("full_load: week load limit error!")
        self._full_load = value
