from scheduling.domain.group_schedule import *
from random import randint


class RoomInfo(Room):
    def __init__(self, room, wday, class_n, busy):
        Room.__init__(self, room.room_num, room.room_type)
        self.wday = wday
        self.class_n = class_n
        self.busy = busy

    def set_busy(self):
        self.busy = 1


class TeacherInfo(Teacher):
    def __init__(self, teacher, wday, class_n, busy):
        Teacher.__init__(self, teacher.id, teacher.surname, teacher.name, teacher.mid_name, teacher.subjects)
        self.full_load = teacher.full_load
        self.wday = wday
        self.class_n = class_n
        self.busy = busy

    def set_busy(self):
        self.busy = 1


class GroupInfo(Group):
    def __init__(self, group, wday, class_n, busy):
        Group.__init__(self, group.id, group.title, group.plan)
        self.wday = wday
        self.class_n = class_n
        self.busy = busy

    def set_busy(self):
        self.busy = 1


class Schedule(object):

    def __init__(self, id, groups=None, teachers=None, rooms=None):
        self.id = id
        self.group_schedules = []
        if groups is None and teachers is None and rooms is None:
            return
        # save all the classes into list
        all_classes = []
        # list of rooms with info
        room_list = []
        for i in range(len(rooms)):
            for j in Weekday:
                for k in range(DAILY_CLASSES_LIMIT):
                    room_list.append(RoomInfo(rooms[i], Weekday(j), k+1, 0))

        # list of teachers with info
        teach_list = []
        for i in range(len(teachers)):
            for j in Weekday:
                for k in range(DAILY_CLASSES_LIMIT):
                    teach_list.append(TeacherInfo(teachers[i], Weekday(j), k+1, 0))

        # list of groups with info
        group_list = []
        for i in range(len(groups)):
            for j in Weekday:
                for k in range(DAILY_CLASSES_LIMIT):
                    group_list.append(GroupInfo(groups[i], Weekday(j), k+1, 0))
        i = 0
        while i < len(groups):
            group = groups[i]
            for k in range(len(group.plan.subjects)):
                g_ok, t_ok, r_ok = 0, 0, 0
                for j in range(len(teachers)):
                    teacher = teachers[j]
                    for l in range(len(teacher.subjects)):
                        # count load of the teacher and subject
                        teacher_load = sum(1 for ind in range(len(teach_list)) if
                                           teach_list[ind].id == teacher.id and
                                           teach_list[ind].busy == 1)
                        subject_load = group.plan.subjects[k].lab_load + \
                                       group.plan.subjects[k].lec_load + \
                                       group.plan.subjects[k].sem_load
                        if (group.plan.subjects[k].id == teacher.subjects[l].id) \
                                and (subject_load + teacher_load <= teacher.full_load):
                            subj_classes = []
                            # generate classes for labs
                            for lab in range(group.plan.subjects[k].lab_load):
                                g_ok, t_ok, r_ok = 0, 0, 0
                                i_try = 0
                                while g_ok == 0 or t_ok == 0 or r_ok == 0:
                                    i_try += 1
                                    # random weekday
                                    wday = randint(0, 5)
                                    # random class number
                                    class_n = randint(1, 5)
                                    # check load of the group for a day
                                    group_wday_load = len([ind for ind in range(len(group_list)) if
                                                           group_list[ind].id == group.id and
                                                           group_list[ind].wday == Weekday(wday) and
                                                           group_list[ind].busy == 1])
                                    if group_wday_load < DAILY_CLASSES_LIMIT:
                                        for g in range(len(group_list)):
                                            if group_list[g].id == group.id and group_list[g].wday == Weekday(wday) \
                                                    and group.plan.subjects[k] in group_list[g].plan.subjects \
                                                    and group_list[g].class_n == class_n \
                                                    and group_list[g].busy == 0:
                                                group_list[g].set_busy()
                                                g_ok = 1
                                                gr = group_list[g]
                                        for t in range(len(teach_list)):
                                            if teach_list[t].id == teacher.id and teach_list[t].wday == Weekday(wday) \
                                                    and group.plan.subjects[k] in teach_list[t].subjects \
                                                    and teach_list[t].class_n == class_n \
                                                    and teach_list[t].busy == 0:
                                                teach_list[t].set_busy()
                                                t_ok = 1
                                                tch = teach_list[t]
                                        # check all the rooms
                                        for m in range(len(room_list)):
                                            # find empty rooms with type LAB
                                            if room_list[m].room_type == Activity.LAB \
                                                    and room_list[m].wday == Weekday(wday) \
                                                    and room_list[m].class_n == class_n \
                                                    and room_list[m].busy == 0:
                                                room_list[m].set_busy()
                                                r_ok = 1
                                                room = room_list[m]
                                                break
                                    if t_ok == 1 and g_ok == 1 and r_ok == 1:
                                        st_class = StudyClass(Weekday(wday), class_n,
                                                              gr.plan.subjects[k], tch, gr, room)
                                        subj_classes.append(st_class)
                                    else:
                                        if i_try == 2*len(Weekday):
                                            if r_ok == 0:
                                                raise Exception("schedule: not enough rooms for labs!")
                                            if g_ok == 0:
                                                raise Exception("schedule: too many classes for the group!")
                                            if t_ok == 0:
                                                break
                            # generate classes for lectures
                            for lec in range(group.plan.subjects[k].lec_load):
                                g_ok, t_ok, r_ok = 0, 0, 0
                                i_try = 0
                                while g_ok == 0 or t_ok == 0 or r_ok == 0:
                                    i_try += 1
                                    # random weekday
                                    wday = randint(0, 5)
                                    #random class number
                                    class_n = randint(1, 5)
                                    # check load of the group for a day
                                    group_wday_load = len([ind for ind in range(len(group_list)) if
                                                           group_list[ind].id == group.id and
                                                           group_list[ind].wday == Weekday(wday) and
                                                           group_list[ind].busy == 1])
                                    if group_wday_load < DAILY_CLASSES_LIMIT:
                                        for g in range(len(group_list)):
                                            if group_list[g].id == group.id and group_list[g].wday == Weekday(wday) \
                                                    and group.plan.subjects[k] in group_list[g].plan.subjects \
                                                    and group_list[g].class_n == class_n \
                                                    and group_list[g].busy == 0:
                                                group_list[g].set_busy()
                                                g_ok = 1
                                                gr = group_list[g]
                                        for t in range(len(teach_list)):
                                            if teach_list[t].id == teacher.id and teach_list[t].wday == Weekday(wday) \
                                                    and group.plan.subjects[k] in teach_list[t].subjects \
                                                    and teach_list[t].class_n == class_n \
                                                    and teach_list[t].busy == 0:
                                                teach_list[t].set_busy()
                                                t_ok = 1
                                                tch = teach_list[t]
                                        # check all the rooms
                                        for m in range(len(room_list)):
                                            # find empty rooms with type LEC
                                            if room_list[m].room_type == Activity.LEC \
                                                    and room_list[m].wday == Weekday(wday) \
                                                    and room_list[m].class_n == class_n \
                                                    and room_list[m].busy == 0:
                                                room_list[m].set_busy()
                                                r_ok = 1
                                                room = room_list[m]
                                                break
                                    if t_ok == 1 and g_ok == 1 and r_ok == 1:
                                        st_class = StudyClass(Weekday(wday), class_n,
                                                              gr.plan.subjects[k], tch, gr, room)
                                        subj_classes.append(st_class)
                                    else:
                                        if i_try == 2*len(Weekday):
                                            if r_ok == 0:
                                                raise Exception("schedule: not enough rooms for lectures!")
                                            if g_ok == 0:
                                                raise Exception("schedule: too many classes for the group!")
                                            if t_ok == 0:
                                                break
                            # generate classes for seminars
                            for sem in range(group.plan.subjects[k].sem_load):
                                g_ok, t_ok, r_ok = 0, 0, 0
                                i_try = 0
                                while g_ok == 0 or t_ok == 0 or r_ok == 0:
                                    i_try += 1
                                    # random weekday
                                    wday = randint(0, 5)
                                    # random class number
                                    class_n = randint(1, 5)
                                    # check load of the group for a day
                                    group_wday_load = len([ind for ind in range(len(group_list)) if
                                                           group_list[ind].id == group.id and
                                                           group_list[ind].wday == Weekday(wday) and
                                                           group_list[ind].busy == 1])
                                    if group_wday_load < DAILY_CLASSES_LIMIT:
                                        for g in range(len(group_list)):
                                            if group_list[g].id == group.id and group_list[g].wday == Weekday(wday) \
                                                    and group.plan.subjects[k] in group_list[g].plan.subjects \
                                                    and group_list[g].class_n == class_n \
                                                    and group_list[g].busy == 0:
                                                group_list[g].set_busy()
                                                g_ok = 1
                                                gr = group_list[g]
                                        for t in range(len(teach_list)):
                                            if teach_list[t].id == teacher.id and teach_list[t].wday == Weekday(wday) \
                                                    and group.plan.subjects[k] in teach_list[t].subjects \
                                                    and teach_list[t].class_n == class_n \
                                                    and teach_list[t].busy == 0:
                                                teach_list[t].set_busy()
                                                t_ok = 1
                                                tch = teach_list[t]
                                        # check all the rooms
                                        for m in range(len(room_list)):
                                            # find empty rooms with type SEM
                                            if room_list[m].room_type == Activity.SEM \
                                                    and room_list[m].wday == Weekday(wday) \
                                                    and room_list[m].class_n == class_n \
                                                    and room_list[m].busy == 0:
                                                room_list[m].set_busy()
                                                r_ok = 1
                                                room = room_list[m]
                                                break
                                    if t_ok == 1 and g_ok == 1 and r_ok == 1:
                                        st_class = StudyClass(Weekday(wday), class_n,
                                                          gr.plan.subjects[k], tch, gr, room)
                                        subj_classes.append(st_class)
                                    else:
                                        if i_try == 2*len(Weekday):
                                            if r_ok == 0:
                                                raise Exception("schedule: not enough rooms for seminars!")
                                            if g_ok == 0:
                                                raise Exception("schedule: too many classes for the group!")
                                            if t_ok == 0:
                                                break
                            if len(subj_classes) == subject_load:
                                for ind in range(len(subj_classes)):
                                    all_classes.append(subj_classes[ind])
                                    # print subj_classes[ind].group.title, subj_classes[ind].group.plan.subjects[k].title,\
                                    #    subj_classes[ind].teacher.surname, \
                                    #    subj_classes[ind].weekday, subj_classes[ind].position, \
                                    #    subj_classes[ind].room.room_num
                            # go to the next teacher
                            else:
                                j += 1
                    # already found a teacher for subject
                    if t_ok == 1 and g_ok == 1 and r_ok == 1:
                        break
                if t_ok == 0:
                    raise Exception("schedule: not enough teachers!")
                if g_ok == 0:
                    raise Exception("schedule: too many classes for the group!")
                # already created a class with this subject
                if t_ok == 1 and g_ok == 1 and r_ok == 1:
                    k += 1
            i += 1
        # sort all_classes by groups
        for i in range(len(groups)):
            gr_sched = GroupSchedule(groups[i])
            for j in range(len(all_classes)):
                if all_classes[j].group.id == groups[i].id:
                    gr_sched.add_study_class(all_classes[j])
            self.add_group_schedule(gr_sched)

    def add_group_schedule(self, value):
        self.group_schedules.append(value)

    def get_gr_schedule(self, id):
        for i in range(len(self.group_schedules)):
            if self.group_schedules[i].group.id == id:
                return self.group_schedules[i]
