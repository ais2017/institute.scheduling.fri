from scheduling.domain.schedule import *
import uuid


class Branch(object):
    def __init__(self, id, title):
        self.id = id
        self.title = title
        self.room_list = []
        self.subject_list = []
        self.plan_list = []
        self.group_list = []
        self.teacher_list = []
        self.draft_schedule = None
        self.cur_schedule = None
        self.schedule_list = []

    def add_room(self, room_num, room_type):
        room = Room(room_num, room_type)
        self.room_list.append(room)

    def delete_room(self, room_num):
        for i in range(len(self.room_list)):
            if self.room_list[i].room_num == room_num:
                del self.room_list[i]
                break

    def add_subject(self, id, title, lab_load, lec_load, sem_load):
        subject = Subject(id, title, lab_load, lec_load, sem_load)
        self.subject_list.append(subject)

    def delete_subject(self, id):
        for i in range(len(self.subject_list)):
            if self.subject_list[i].id == id:
                del self.subject_list[i]
                break

    def add_plan(self, id, title, subj_ids):
        subjects = []
        for i in range(len(subj_ids)):
            for j in range(len(self.subject_list)):
                if self.subject_list[j].id == subj_ids[i]:
                    subjects.append(self.subject_list[j])
        if len(subjects) == 0:
            raise Exception("add_plan: wrong subject id")
        plan = Plan(id, title, subjects)
        self.plan_list.append(plan)

    def delete_plan(self, id):
        for i in range(len(self.plan_list)):
            if self.plan_list[i].id == id:
                del self.plan_list[i]
                break

    def add_group(self, id, title, plan_id):
        plan = None
        for i in range(len(self.plan_list)):
            if self.plan_list[i].id == plan_id:
                plan = self.plan_list[i]
                break
        if plan is None:
            raise Exception("add_group: plan not found!")
        group = Group(id, title, plan)
        self.group_list.append(group)

    def delete_group(self, id):
        for i in range(len(self.group_list)):
            if self.group_list[i].id == id:
                del self.group_list[i]
                break

    def add_teacher(self, id, surname, name, mid_name, subj_ids):
        subjects = []
        for i in range(len(subj_ids)):
            for j in range(len(self.subject_list)):
                if self.subject_list[j].id == subj_ids[i]:
                    subjects.append(self.subject_list[j])
        if len(subjects) == 0:
            raise Exception("add_teacher: wrong subject id")
        teacher = Teacher(id, surname, name, mid_name, subjects)
        self.teacher_list.append(teacher)

    def delete_teacher(self, id):
        for i in range(len(self.teacher_list)):
            if self.teacher_list[i].id == id:
                del self.teacher_list[i]
                break

    def create_schedule(self):
        del self.schedule_list[:]
        if len(self.group_list) == 0 or len(self.teacher_list) == 0 or self.room_list == 0:
            raise Exception("create_schedule: not enough info!")
        for i in range(SCHEDULES_NUM):
            sched = Schedule(str(uuid.uuid4()), self.group_list, self.teacher_list, self.room_list)
            self.schedule_list.append(sched)

    def send_schedule(self, id):
        if len(self.schedule_list) == 0:
            raise Exception("send_schedule: no any schedules!")
        f = 0
        for i in range(len(self.schedule_list)):
            if self.schedule_list[i].id == id:
                self.draft_schedule = self.schedule_list[i]
                f = 1
                break
        if f == 0:
            raise Exception("send_schedule: no such schedule!")

    def confirm_schedule(self):
        if self.draft_schedule is None:
            raise Exception("confirm_schedule: no any draft schedules!")
        self.cur_schedule = self.draft_schedule

    def get_schedule(self):
        return self.cur_schedule

    def get_draft_schedule(self):
        return self.draft_schedule
