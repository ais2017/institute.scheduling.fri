from scheduling.domain.Global import *
from scheduling.domain.subject import *
from scheduling.domain.teacher import *
from scheduling.domain.room import *
from scheduling.domain.group import *
from enum import IntEnum, unique
# if enum is not working : pip install enum34


# day of the week
@unique
class Weekday(IntEnum):
    MON = 0
    TUE = 1
    WED = 2
    THU = 3
    FRI = 4
    SAT = 5


class StudyClass(object):
    def __init__(self, weekday, position, subject, teacher, group, room):
        self.weekday = weekday
        self.position = position
        self.subject = subject
        self.teacher = teacher
        self.group = group
        self.room = room

    @property
    def weekday(self):
        return self._weekday

    @weekday.setter
    def weekday(self, value):
        if not isinstance(value, Weekday):
            raise Exception("weekday: invalid day of the week!")
        self._weekday = value

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, value):
        if (value <= 0) or (value > DAILY_CLASSES_LIMIT):
            raise Exception("position: invalid class position!")
        self._position = value

    @property
    def subject(self):
        return self._subject

    @subject.setter
    def subject(self, value):
        if not isinstance(value, Subject):
            raise Exception("subject: invalid subject!")
        self._subject = value

    @property
    def teacher(self):
        return self._teacher

    @teacher.setter
    def teacher(self, value):
        if not isinstance(value, Teacher):
            raise Exception("teacher: invalid teacher!")
        # check teacher and subject compatibility
        if self.subject not in value.subjects:
            raise Exception("teacher: subject is not supported!")
        self._teacher = value

    @property
    def group(self):
        return self._group

    @group.setter
    def group(self, value):
        if not isinstance(value, Group):
            raise Exception("group: invalid group!")
        # check group plan
        if self.subject not in value.plan.subjects:
            raise Exception("group: subject is not supported!")
        self._group = value

    @property
    def room(self):
        return self._room

    @room.setter
    def room(self, value):
        if not isinstance(value, Room):
            raise Exception("room: invalid room!")
        self._room = value
