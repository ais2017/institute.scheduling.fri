import json
from scheduling.domain.branch import *


# recursive serialization of nested objects
class CustomJsonEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, (Branch, Group, GroupSchedule, Plan,
                          Room, Schedule, StudyClass, Subject, Teacher)):
            # remove "_" for the private attributes instead of using __dict__
            return {k.lstrip('_'): v.name if isinstance(v, IntEnum)
                    else v for k, v in vars(o).items()}
        else:
            return json.JSONEncoder.encode(self, o)