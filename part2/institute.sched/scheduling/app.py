# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui
from scheduling.ui.mainwindow import Ui_scheduling_system
from scheduling.use_case.use_case import *
from scheduling.database.psql_gateway import PSQLGateway
import uuid
import sys
import json


class Application(QtGui.QMainWindow, Ui_scheduling_system):
    def __init__(self):
        super(self.__class__, self).__init__()
        self.db = PSQLGateway('pq://test_user:1111@localhost:5432/scheduling')
        self.setupUi(self)
        # actual state of the branches
        self.branches_list_json = json.loads(load_current_info(self.db)[1])
        # created schedules list
        self.schedules_json = None
        self.branches_db = []
        self.created_schedule_id = 0
        # set tables init state
        self.update_branch_info()
        if len(self.branches_db) != 0:
            self.set_room_table(self.branches_db[0]['id'])
            self.set_subject_table(self.branches_db[0]['id'])
            self.set_teacher_table(self.branches_db[0]['id'])
            self.set_plan_table(self.branches_db[0]['id'])
            self.set_group_table(self.branches_db[0]['id'])
            self.set_subjects_dropdown_teacher()
            self.set_subjects_dropdown_plan()
            self.set_plans_dropdown_menu()
            self.set_groups_dropdown_create()
            self.set_groups_dropdown_confirm()
            self.set_groups_dropdown_watch()
            self.view_draft_schedule()
            self.view_current_schedule()
        self.set_controls()

    def set_controls(self):
        # branch
        self.add_branch.clicked.connect(lambda: self.add_branch_action())
        self.delete_branch.clicked.connect(lambda: self.delete_branch_action())
        # room
        self.add_room.clicked.connect(lambda: self.add_room_action())
        self.branch_rooms.activated.connect(lambda: self.set_room_table(
            self.branches_db[self.branch_rooms.currentIndex()]['id']))
        self.delete_room.clicked.connect(lambda: self.delete_room_action())
        # subject
        self.add_subject.clicked.connect(lambda: self.add_subject_action())
        self.branch_subjects.activated.connect(lambda: self.set_subject_table(
            self.branches_db[self.branch_subjects.currentIndex()]['id']))
        self.delete_subject.clicked.connect(lambda: self.delete_subject_action())
        # teacher
        self.add_teacher.clicked.connect(lambda: self.add_teacher_action())
        self.branch_teachers.activated.connect(lambda: self.set_subjects_dropdown_teacher())
        self.add_subj_teacher.clicked.connect(lambda: self.add_subject_to_teacher())
        self.branch_teachers.activated.connect(lambda: self.set_teacher_table(
            self.branches_db[self.branch_teachers.currentIndex()]['id']))
        self.show_subjects_teacher.clicked.connect(lambda: self.show_teacher_subjects())
        self.delete_teacher.clicked.connect(lambda: self.delete_teacher_action())
        # plan
        self.add_plan.clicked.connect(lambda: self.add_plan_action())
        self.branch_plans.activated.connect(lambda: self.set_subjects_dropdown_plan())
        self.add_subj_plan.clicked.connect(lambda: self.add_subject_to_plan())
        self.branch_plans.activated.connect(lambda: self.set_plan_table(
            self.branches_db[self.branch_plans.currentIndex()]['id']))
        self.show_subj_plan.clicked.connect(lambda: self.show_plan_subjects())
        self.delete_plan.clicked.connect(lambda: self.delete_plan_action())
        # group
        self.add_group.clicked.connect(lambda: self.add_group_action())
        self.branch_groups.activated.connect(lambda: self.set_group_table(
            self.branches_db[self.branch_groups.currentIndex()]['id']))
        self.delete_group.clicked.connect(lambda: self.delete_group_action())
        self.branch_groups.activated.connect(lambda: self.set_plans_dropdown_menu())
        # schedule
        self.create_schedule.clicked.connect(lambda: self.create_schedule_action())
        self.next_schedule.clicked.connect(lambda: self.view_next_schedule())
        self.branch_create.activated.connect(lambda: self.set_groups_dropdown_create())
        self.group_create.activated.connect(lambda: self.view_created_schedule())
        self.add_group.clicked.connect(lambda: self.set_groups_dropdown_create())
        self.add_group.clicked.connect(lambda: self.set_groups_dropdown_confirm())
        self.add_group.clicked.connect(lambda: self.set_groups_dropdown_watch())
        self.send_schedule.clicked.connect(lambda: self.send_schedule_action())
        self.branch_confirm.activated.connect(lambda: self.set_groups_dropdown_confirm())
        self.branch_confirm.activated.connect(lambda: self.view_draft_schedule())
        self.group_confirm.activated.connect(lambda: self.view_draft_schedule())
        self.confirm_schedule.clicked.connect(lambda: self.confirm_schedule_action())
        self.branch_watch.activated.connect(lambda: self.set_groups_dropdown_watch())
        self.branch_watch.activated.connect(lambda: self.view_current_schedule())
        self.group_watch.activated.connect(lambda: self.view_current_schedule())

    def set_branch_dropdown_menu(self, menu):
        menu.clear()
        for i in range(len(self.branches_db)):
            menu.addItem(self.branches_db[i]['title'])

    # add checkboxes to the table
    def update_check_items(self):
        # branches
        for i in range(self.branch_list.rowCount()):
            item = QtGui.QRadioButton()
            item.setChecked(False)
            self.branch_list.setCellWidget(i, 1, item)
        # rooms
        for i in range(self.room_list.rowCount()):
            item = QtGui.QRadioButton()
            item.setChecked(False)
            self.room_list.setCellWidget(i, 2, item)
        # subjects
        for i in range(self.subjects_list.rowCount()):
            item = QtGui.QRadioButton()
            item.setChecked(False)
            self.subjects_list.setCellWidget(i, 4, item)
        # teachers
        for i in range(self.teacher_list.rowCount()):
            item = QtGui.QRadioButton()
            item.setChecked(False)
            self.teacher_list.setCellWidget(i, 3, item)
        # plans
        for i in range(self.plan_list.rowCount()):
            item = QtGui.QRadioButton()
            item.setChecked(False)
            self.plan_list.setCellWidget(i, 1, item)
        # groups
        for i in range(self.group_list.rowCount()):
            item = QtGui.QRadioButton()
            item.setChecked(False)
            self.group_list.setCellWidget(i, 2, item)

    def set_subjects_dropdown_teacher(self):
        self.teacher_subj.clear()
        self.added_teach_subj_list.clear()
        ind = self.branch_teachers.currentIndex()
        subjects = json.loads(get_all_subjects(self.branches_db[ind]['id'])[1])
        for i in range(len(subjects)):
            self.teacher_subj.addItem(subjects[i]['title'])

    def set_subjects_dropdown_plan(self):
        self.plan_subj.clear()
        self.added_plan_subj_list.clear()
        ind = self.branch_plans.currentIndex()
        subjects = json.loads(get_all_subjects(self.branches_db[ind]['id'])[1])
        for i in range(len(subjects)):
            self.plan_subj.addItem(subjects[i]['title'])

    def set_plans_dropdown_menu(self):
        self.group_plan.clear()
        ind = self.branch_groups.currentIndex()
        plans = json.loads(get_all_plans(self.branches_db[ind]['id'])[1])
        for i in range(len(plans)):
            self.group_plan.addItem(plans[i]['title'])

    def set_groups_dropdown_create(self):
        self.group_create.clear()
        ind = self.branch_create.currentIndex()
        groups = json.loads(get_all_groups(self.branches_db[ind]['id'])[1])
        for i in range(len(groups)):
            self.group_create.addItem(groups[i]['title'])

    def set_groups_dropdown_confirm(self):
        self.group_confirm.clear()
        ind = self.branch_confirm.currentIndex()
        groups = json.loads(get_all_groups(self.branches_db[ind]['id'])[1])
        for i in range(len(groups)):
            self.group_confirm.addItem(groups[i]['title'])

    def set_groups_dropdown_watch(self):
        self.group_watch.clear()
        ind = self.branch_watch.currentIndex()
        groups = json.loads(get_all_groups(self.branches_db[ind]['id'])[1])
        for i in range(len(groups)):
            self.group_watch.addItem(groups[i]['title'])

    def set_branches_table(self):
        self.branch_list.setRowCount(len(self.branches_db))
        for i in range(self.branch_list.rowCount()):
            self.branch_list.setItem(i, 0, QtGui.QTableWidgetItem(self.branches_db[i]['title']))
        self.update_check_items()

    # set dropdown menus and branch list in the actual state
    def update_branch_info(self):
        self.branches_db = json.loads(get_all_branches()[1])
        self.set_branches_table()
        self.set_branch_dropdown_menu(self.branch_rooms)
        self.set_branch_dropdown_menu(self.branch_confirm)
        self.set_branch_dropdown_menu(self.branch_create)
        self.set_branch_dropdown_menu(self.branch_groups)
        self.set_branch_dropdown_menu(self.branch_plans)
        self.set_branch_dropdown_menu(self.branch_subjects)
        self.set_branch_dropdown_menu(self.branch_teachers)
        self.set_branch_dropdown_menu(self.branch_watch)

    def add_branch_action(self):
        try:
            self.info_box.setText('')
            if self.branch_title.text() == '':
                self.info_box.setText(u'Введите название филиала!')
                return
            ret = add_branch(self.db, str(uuid.uuid4()), self.branch_title.text())
            self.info_box.setText(ret[1])
            self.branch_title.setText('')
            self.update_branch_info()
        except Exception as e:
            self.info_box.setText(str(e))

    def set_room_table(self, branch_id):
        rooms = json.loads(get_all_rooms(branch_id)[1])
        self.room_list.setRowCount(len(rooms))
        for i in range(self.room_list.rowCount()):
            self.room_list.setItem(i, 0, QtGui.QTableWidgetItem(str(rooms[i]['room_num'])))
            if rooms[i]['room_type'] == 'LAB':
                self.room_list.setItem(i, 1, QtGui.QTableWidgetItem("лаборатория"))
            if rooms[i]['room_type'] == 'LEC':
                self.room_list.setItem(i, 1, QtGui.QTableWidgetItem("лекционная"))
            if rooms[i]['room_type'] == 'SEM':
                self.room_list.setItem(i, 1, QtGui.QTableWidgetItem("семинар"))
        self.update_check_items()

    def add_room_action(self):
        try:
            ind = self.branch_rooms.currentIndex()
            self.info_box.setText('')
            if self.room_num.text() == '':
                self.info_box.setText(u'Введите номер аудитории!')
                return
            ret = add_room(self.db, self.branches_db[ind]['id'],
                           int(self.room_num.text()), self.room_type.currentIndex())
            self.info_box.setText(ret[1])
            self.room_num.setText('')
            self.set_room_table(self.branches_db[ind]['id'])
        except Exception as e:
            self.info_box.setText(str(e))

    def set_subject_table(self, branch_id):
        subjects = json.loads(get_all_subjects(branch_id)[1])
        self.subjects_list.setRowCount(len(subjects))
        for i in range(self.subjects_list.rowCount()):
            self.subjects_list.setItem(i, 0, QtGui.QTableWidgetItem(str(subjects[i]['title'])))
            self.subjects_list.setItem(i, 1, QtGui.QTableWidgetItem(str(subjects[i]['lab_load'])))
            self.subjects_list.setItem(i, 2, QtGui.QTableWidgetItem(str(subjects[i]['lec_load'])))
            self.subjects_list.setItem(i, 3, QtGui.QTableWidgetItem(str(subjects[i]['sem_load'])))
        self.update_check_items()

    def add_subject_action(self):
        try:
            ind = self.branch_subjects.currentIndex()
            self.info_box.setText('')
            if self.subject_title.text() == '':
                self.info_box.setText(u'Введите название предмета!')
                return
            if self.subject_lab.text() == '':
                self.info_box.setText(u'Введите кол-во лабораторных занятий!')
                return
            if self.subject_lec.text() == '':
                self.info_box.setText(u'Введите кол-во лекций!')
                return
            if self.subject_sem.text() == '':
                self.info_box.setText(u'Введите кол-во семинаров!')
                return
            ret = add_subject(self.db, self.branches_db[ind]['id'],
                              str(uuid.uuid4()),
                              self.subject_title.text(),
                              int(self.subject_lab.text()),
                              int(self.subject_lec.text()),
                              int(self.subject_sem.text()))
            self.info_box.setText(ret[1])
            self.subject_title.setText('')
            self.subject_lab.setText('')
            self.subject_lec.setText('')
            self.subject_sem.setText('')
            self.set_subject_table(self.branches_db[ind]['id'])
            self.set_subjects_dropdown_teacher()
            self.set_subjects_dropdown_plan()
        except Exception as e:
            self.info_box.setText(str(e))

    def add_subject_to_teacher(self):
        self.info_box.setText('')
        subj_title = self.teacher_subj.currentText()
        if self.added_teach_subj_list.findItems(subj_title, QtCore.Qt.MatchExactly):
            self.info_box.setText(u'Вы уже добавили такой предмет!')
            return
        self.added_teach_subj_list.addItem(subj_title)

    def set_teacher_table(self, branch_id):
        teachers = json.loads(get_all_teachers(branch_id)[1])
        self.teacher_list.setRowCount(len(teachers))
        for i in range(self.teacher_list.rowCount()):
            self.teacher_list.setItem(i, 0, QtGui.QTableWidgetItem(str(teachers[i]['surname'])))
            self.teacher_list.setItem(i, 1, QtGui.QTableWidgetItem(str(teachers[i]['name'])))
            self.teacher_list.setItem(i, 2, QtGui.QTableWidgetItem(str(teachers[i]['mid_name'])))
        self.update_check_items()

    def add_teacher_action(self):
        try:
            self.info_box.setText('')
            if self.teacher_surname.text() == '':
                self.info_box.setText(u'Введите фамилию преподавателя!')
                return
            if self.teacher_name.text() == '':
                self.info_box.setText(u'Введите имя преподавателя!')
                return
            if self.teacher_midname.text() == '':
                self.info_box.setText(u'Введите отчество преподавателя!')
                return
            if self.added_teach_subj_list.count() == 0:
                self.info_box.setText(u'Добавьте предметы!')
                return
            # save subject list
            teach_subjects = []
            for i in range(self.added_teach_subj_list.count()):
                teach_subjects.append(self.added_teach_subj_list.item(i).text())
            ind = self.branch_teachers.currentIndex()
            ret = add_teacher(self.db, self.branches_db[ind]['id'], str(uuid.uuid4()),
                              self.teacher_surname.text(),
                              self.teacher_name.text(),
                              self.teacher_midname.text(),
                              teach_subjects)
            self.info_box.setText(ret[1])
            self.teacher_name.setText('')
            self.teacher_surname.setText('')
            self.teacher_midname.setText('')
            self.added_teach_subj_list.clear()
            self.set_teacher_table(self.branches_db[ind]['id'])
        except Exception as e:
            self.info_box.setText(str(e))

    def add_subject_to_plan(self):
        self.info_box.setText('')
        subj_title = self.plan_subj.currentText()
        if self.added_plan_subj_list.findItems(subj_title, QtCore.Qt.MatchExactly):
            self.info_box.setText(u'Вы уже добавили такой предмет!')
            return
        self.added_plan_subj_list.addItem(subj_title)

    def set_plan_table(self, branch_id):
        plans = json.loads(get_all_plans(branch_id)[1])
        self.plan_list.setRowCount(len(plans))
        for i in range(self.plan_list.rowCount()):
            self.plan_list.setItem(i, 0, QtGui.QTableWidgetItem(str(plans[i]['title'])))
        self.update_check_items()

    def add_plan_action(self):
        try:
            self.info_box.setText('')
            if self.plan_title.text() == '':
                self.info_box.setText(u'Введите название учебного плана!')
                return
            if self.added_plan_subj_list.count() == 0:
                self.info_box.setText(u'Добавьте предметы!')
                return
            # save subject list
            plan_subjects = []
            for i in range(self.added_plan_subj_list.count()):
                plan_subjects.append(self.added_plan_subj_list.item(i).text())
            ind = self.branch_plans.currentIndex()
            ret = add_plan(self.db, self.branches_db[ind]['id'], str(uuid.uuid4()),
                           self.plan_title.text(), plan_subjects)
            self.info_box.setText(ret[1])
            self.plan_title.setText('')
            self.added_plan_subj_list.clear()
            self.set_plan_table(self.branches_db[ind]['id'])
            self.set_plans_dropdown_menu()
        except Exception as e:
            self.info_box.setText(str(e))

    def set_group_table(self, branch_id):
        groups = json.loads(get_all_groups(branch_id)[1])
        plans = json.loads(get_all_plans(branch_id)[1])
        self.group_list.setRowCount(len(groups))
        for i in range(self.group_list.rowCount()):
            self.group_list.setItem(i, 0, QtGui.QTableWidgetItem(str(groups[i]['title'])))
            for j in plans:
                if j['id'] == groups[i]['plan']['id']:
                    self.group_list.setItem(i, 1, QtGui.QTableWidgetItem(str(j['title'])))
        self.update_check_items()

    def add_group_action(self):
        try:
            ind = self.branch_groups.currentIndex()
            self.info_box.setText('')
            if self.group_title.text() == '':
                self.info_box.setText(u'Введите название группы!')
                return
            plans = json.loads(get_all_plans(self.branches_db[ind]['id'])[1])
            ret = add_group(self.db, self.branches_db[ind]['id'], str(uuid.uuid4()),
                            self.group_title.text(), plans[self.group_plan.currentIndex()]['title'])
            self.info_box.setText(ret[1])
            self.group_title.setText('')
            self.set_group_table(self.branches_db[ind]['id'])
        except Exception as e:
            self.info_box.setText(str(e))

    def show_teacher_subjects(self):
        self.info_box.setText('')
        self.teacher_subjects_list.clear()
        ind = self.branch_teachers.currentIndex()
        teachers = json.loads(get_all_teachers(self.branches_db[ind]['id'])[1])
        for i in range(self.teacher_list.rowCount()):
            if self.teacher_list.cellWidget(i, 3).isChecked():
                for j in teachers[i]['subjects']:
                    self.teacher_subjects_list.addItem(j['title'])

    def show_plan_subjects(self):
        self.info_box.setText('')
        self.plan_subj_list.clear()
        ind = self.branch_plans.currentIndex()
        plans = json.loads(get_all_plans(self.branches_db[ind]['id'])[1])
        for i in range(self.plan_list.rowCount()):
            if self.plan_list.cellWidget(i, 1).isChecked():
                for j in plans[i]['subjects']:
                    self.plan_subj_list.addItem(j['title'])

    def delete_branch_action(self):
        # TODO
        self.info_box.setText(u'Ошибка: Удаление филиалов не реализовано!')

    def delete_room_action(self):
        # TODO
        self.info_box.setText(u'Ошибка: Удаление аудиторий не реализовано!')

    def delete_subject_action(self):
        # TODO
        self.info_box.setText(u'Ошибка: Удаление предметов не реализовано!')

    def delete_teacher_action(self):
        # TODO
        self.info_box.setText(u'Ошибка: Удаление преподавателей не реализовано!')

    def delete_plan_action(self):
        # TODO
        self.info_box.setText(u'Ошибка: Удаление учебного плана не реализовано!')

    def delete_group_action(self):
        # TODO
        self.info_box.setText(u'Ошибка: Удаление группы не реализовано!')

    def create_schedule_action(self):
        try:
            self.info_box.setText('')
            self.created_schedule_id = 0
            ret = create_schedule(self.db, self.branches_db[self.branch_create.currentIndex()]['id'])
            if ret[0] != 0:
                self.info_box.setText(ret[1])
                return
            self.schedules_json = json.loads(ret[1])
            self.view_created_schedule()
            self.info_box.setText(ret[2])
        except Exception as e:
            self.info_box.setText(str(e))

    def view_next_schedule(self):
        if self.created_schedule_id == len(self.schedules_json)-1:
            self.created_schedule_id = 0
        else:
            self.created_schedule_id += 1
        self.view_created_schedule()

    def view_created_schedule(self):
        self.mon_table_create.clearContents()
        self.tue_table_create.clearContents()
        self.wed_table_create.clearContents()
        self.thu_table_create.clearContents()
        self.fri_table_create.clearContents()
        self.sat_table_create.clearContents()
        if self.schedules_json is None:
            self.info_box.setText(u'Ошибка: Расписание не составлено!')
            return
        self.info_box.setText(u'Вариант расписания: ' + str(self.created_schedule_id + 1))
        group_title = self.group_create.currentText()
        for i in self.schedules_json[self.created_schedule_id]['group_schedules']:
            if i['group']['title'] == group_title:
                for j in i['classes_list']:
                    if j['weekday'] == 'MON':
                        p = j['position']
                        self.mon_table_create.setItem(p, 0, QtGui.QTableWidgetItem(
                            str(j['subject']['title'])))
                        if j['room']['room_type'] == 'LAB':
                            self.mon_table_create.setItem(p, 1, QtGui.QTableWidgetItem(
                                u'лаб.работа'))
                        if j['room']['room_type'] == 'LEC':
                            self.mon_table_create.setItem(p, 1, QtGui.QTableWidgetItem(
                                u'лекция'))
                        if j['room']['room_type'] == 'SEM':
                            self.mon_table_create.setItem(p, 1, QtGui.QTableWidgetItem(
                                u'семинар'))
                        self.mon_table_create.setItem(p, 2, QtGui.QTableWidgetItem(
                            j['teacher']['surname'] + ' ' +
                            j['teacher']['name'][0] + '.' +
                            j['teacher']['mid_name'][0] + '.'))
                    if j['weekday'] == 'TUE':
                        p = j['position']
                        self.tue_table_create.setItem(p, 0, QtGui.QTableWidgetItem(
                            str(j['subject']['title'])))
                        if j['room']['room_type'] == 'LAB':
                            self.tue_table_create.setItem(p, 1, QtGui.QTableWidgetItem(
                                u'лаб.работа'))
                        if j['room']['room_type'] == 'LEC':
                            self.tue_table_create.setItem(p, 1, QtGui.QTableWidgetItem(
                                u'лекция'))
                        if j['room']['room_type'] == 'SEM':
                            self.tue_table_create.setItem(p, 1, QtGui.QTableWidgetItem(
                                u'семинар'))
                        self.tue_table_create.setItem(p, 2, QtGui.QTableWidgetItem(
                            j['teacher']['surname'] + ' ' +
                            j['teacher']['name'][0] + '.' +
                            j['teacher']['mid_name'][0] + '.'))
                    if j['weekday'] == 'WED':
                        p = j['position']
                        self.wed_table_create.setItem(p, 0, QtGui.QTableWidgetItem(
                            str(j['subject']['title'])))
                        if j['room']['room_type'] == 'LAB':
                            self.wed_table_create.setItem(p, 1, QtGui.QTableWidgetItem(
                                u'лаб.работа'))
                        if j['room']['room_type'] == 'LEC':
                            self.wed_table_create.setItem(p, 1, QtGui.QTableWidgetItem(
                                u'лекция'))
                        if j['room']['room_type'] == 'SEM':
                            self.wed_table_create.setItem(p, 1, QtGui.QTableWidgetItem(
                                u'семинар'))
                        self.wed_table_create.setItem(p, 2, QtGui.QTableWidgetItem(
                            j['teacher']['surname'] + ' ' +
                            j['teacher']['name'][0] + '.' +
                            j['teacher']['mid_name'][0] + '.'))
                    if j['weekday'] == 'THU':
                        p = j['position']
                        self.thu_table_create.setItem(p, 0, QtGui.QTableWidgetItem(
                            str(j['subject']['title'])))
                        if j['room']['room_type'] == 'LAB':
                            self.thu_table_create.setItem(p, 1, QtGui.QTableWidgetItem(
                                u'лаб.работа'))
                        if j['room']['room_type'] == 'LEC':
                            self.thu_table_create.setItem(p, 1, QtGui.QTableWidgetItem(
                                u'лекция'))
                        if j['room']['room_type'] == 'SEM':
                            self.thu_table_create.setItem(p, 1, QtGui.QTableWidgetItem(
                                u'семинар'))
                        self.thu_table_create.setItem(p, 2, QtGui.QTableWidgetItem(
                            j['teacher']['surname'] + ' ' +
                            j['teacher']['name'][0] + '.' +
                            j['teacher']['mid_name'][0] + '.'))
                    if j['weekday'] == 'FRI':
                        p = j['position']
                        self.fri_table_create.setItem(p, 0, QtGui.QTableWidgetItem(
                            str(j['subject']['title'])))
                        if j['room']['room_type'] == 'LAB':
                            self.fri_table_create.setItem(p, 1, QtGui.QTableWidgetItem(
                                u'лаб.работа'))
                        if j['room']['room_type'] == 'LEC':
                            self.fri_table_create.setItem(p, 1, QtGui.QTableWidgetItem(
                                u'лекция'))
                        if j['room']['room_type'] == 'SEM':
                            self.fri_table_create.setItem(p, 1, QtGui.QTableWidgetItem(
                                u'семинар'))
                        self.fri_table_create.setItem(p, 2, QtGui.QTableWidgetItem(
                            j['teacher']['surname'] + ' ' +
                            j['teacher']['name'][0] + '.' +
                            j['teacher']['mid_name'][0] + '.'))
                    if j['weekday'] == 'SAT':
                        p = j['position']
                        self.sat_table_create.setItem(p, 0, QtGui.QTableWidgetItem(
                            str(j['subject']['title'])))
                        if j['room']['room_type'] == 'LAB':
                            self.sat_table_create.setItem(p, 1, QtGui.QTableWidgetItem(
                                u'лаб.работа'))
                        if j['room']['room_type'] == 'LEC':
                            self.sat_table_create.setItem(p, 1, QtGui.QTableWidgetItem(
                                u'лекция'))
                        if j['room']['room_type'] == 'SEM':
                            self.sat_table_create.setItem(p, 1, QtGui.QTableWidgetItem(
                                u'семинар'))
                        self.sat_table_create.setItem(p, 2, QtGui.QTableWidgetItem(
                            j['teacher']['surname'] + ' ' +
                            j['teacher']['name'][0] + '.' +
                            j['teacher']['mid_name'][0] + '.'))

    def send_schedule_action(self):
        try:
            ret = send_schedule(self.db, self.branches_db[self.branch_create.currentIndex()]['id'],
                                self.created_schedule_id)
            self.info_box.setText(ret[1])
            self.view_draft_schedule()
        except Exception as e:
            self.info_box.setText(str(e))

    def confirm_schedule_action(self):
        try:
            ret = confirm_schedule(self.db, self.branches_db[self.branch_create.currentIndex()]['id'])
            self.info_box.setText(ret[1])
            self.view_current_schedule()
        except Exception as e:
            self.info_box.setText(str(e))

    def view_draft_schedule(self):
        self.branches_list_json = json.loads(load_current_info(self.db)[1])
        self.mon_table_confirm.clearContents()
        self.tue_table_confirm.clearContents()
        self.wed_table_confirm.clearContents()
        self.thu_table_confirm.clearContents()
        self.fri_table_confirm.clearContents()
        self.sat_table_confirm.clearContents()
        draft_sched = self.branches_list_json[self.branch_confirm.currentIndex()]['draft_schedule']
        if draft_sched is None:
            return
        group_title = self.group_confirm.currentText()
        i = draft_sched['group_schedules'][self.group_confirm.currentIndex()]
        if i['group']['title'] == group_title:
            for j in i['classes_list']:
                if j['weekday'] == 'MON':
                    p = j['position']
                    self.mon_table_confirm.setItem(p, 0, QtGui.QTableWidgetItem(
                        str(j['subject']['title'])))
                    if j['room']['room_type'] == 'LAB':
                        self.mon_table_confirm.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лаб.работа'))
                    if j['room']['room_type'] == 'LEC':
                        self.mon_table_confirm.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лекция'))
                    if j['room']['room_type'] == 'SEM':
                        self.mon_table_confirm.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'семинар'))
                    self.mon_table_confirm.setItem(p, 2, QtGui.QTableWidgetItem(
                        j['teacher']['surname'] + ' ' +
                        j['teacher']['name'][0] + '.' +
                        j['teacher']['mid_name'][0] + '.'))
                if j['weekday'] == 'TUE':
                    p = j['position']
                    self.tue_table_confirm.setItem(p, 0, QtGui.QTableWidgetItem(
                        str(j['subject']['title'])))
                    if j['room']['room_type'] == 'LAB':
                        self.tue_table_confirm.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лаб.работа'))
                    if j['room']['room_type'] == 'LEC':
                        self.tue_table_confirm.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лекция'))
                    if j['room']['room_type'] == 'SEM':
                        self.tue_table_confirm.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'семинар'))
                    self.tue_table_confirm.setItem(p, 2, QtGui.QTableWidgetItem(
                        j['teacher']['surname'] + ' ' +
                        j['teacher']['name'][0] + '.' +
                        j['teacher']['mid_name'][0] + '.'))
                if j['weekday'] == 'WED':
                    p = j['position']
                    self.wed_table_confirm.setItem(p, 0, QtGui.QTableWidgetItem(
                        str(j['subject']['title'])))
                    if j['room']['room_type'] == 'LAB':
                        self.wed_table_confirm.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лаб.работа'))
                    if j['room']['room_type'] == 'LEC':
                        self.wed_table_confirm.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лекция'))
                    if j['room']['room_type'] == 'SEM':
                        self.wed_table_confirm.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'семинар'))
                    self.wed_table_confirm.setItem(p, 2, QtGui.QTableWidgetItem(
                        j['teacher']['surname'] + ' ' +
                        j['teacher']['name'][0] + '.' +
                        j['teacher']['mid_name'][0] + '.'))
                if j['weekday'] == 'THU':
                    p = j['position']
                    self.thu_table_confirm.setItem(p, 0, QtGui.QTableWidgetItem(
                        str(j['subject']['title'])))
                    if j['room']['room_type'] == 'LAB':
                        self.thu_table_confirm.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лаб.работа'))
                    if j['room']['room_type'] == 'LEC':
                        self.thu_table_confirm.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лекция'))
                    if j['room']['room_type'] == 'SEM':
                        self.thu_table_confirm.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'семинар'))
                    self.thu_table_confirm.setItem(p, 2, QtGui.QTableWidgetItem(
                        j['teacher']['surname'] + ' ' +
                        j['teacher']['name'][0] + '.' +
                        j['teacher']['mid_name'][0] + '.'))
                if j['weekday'] == 'FRI':
                    p = j['position']
                    self.fri_table_confirm.setItem(p, 0, QtGui.QTableWidgetItem(
                        str(j['subject']['title'])))
                    if j['room']['room_type'] == 'LAB':
                        self.fri_table_confirm.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лаб.работа'))
                    if j['room']['room_type'] == 'LEC':
                        self.fri_table_confirm.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лекция'))
                    if j['room']['room_type'] == 'SEM':
                        self.fri_table_confirm.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'семинар'))
                    self.fri_table_confirm.setItem(p, 2, QtGui.QTableWidgetItem(
                        j['teacher']['surname'] + ' ' +
                        j['teacher']['name'][0] + '.' +
                        j['teacher']['mid_name'][0] + '.'))
                if j['weekday'] == 'SAT':
                    p = j['position']
                    self.sat_table_confirm.setItem(p, 0, QtGui.QTableWidgetItem(
                        str(j['subject']['title'])))
                    if j['room']['room_type'] == 'LAB':
                        self.sat_table_confirm.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лаб.работа'))
                    if j['room']['room_type'] == 'LEC':
                        self.sat_table_confirm.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лекция'))
                    if j['room']['room_type'] == 'SEM':
                        self.sat_table_confirm.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'семинар'))
                    self.sat_table_confirm.setItem(p, 2, QtGui.QTableWidgetItem(
                        j['teacher']['surname'] + ' ' +
                        j['teacher']['name'][0] + '.' +
                        j['teacher']['mid_name'][0] + '.'))

    def view_current_schedule(self):
        self.branches_list_json = json.loads(load_current_info(self.db)[1])
        self.mon_table.clearContents()
        self.tue_table.clearContents()
        self.wed_table.clearContents()
        self.thu_table.clearContents()
        self.fri_table.clearContents()
        self.sat_table.clearContents()
        cur_sched = self.branches_list_json[self.branch_watch.currentIndex()]['cur_schedule']
        if cur_sched is None:
            return
        group_title = self.group_watch.currentText()
        i = cur_sched['group_schedules'][self.group_watch.currentIndex()]
        if i['group']['title'] == group_title:
            for j in i['classes_list']:
                if j['weekday'] == 'MON':
                    p = j['position']
                    self.mon_table.setItem(p, 0, QtGui.QTableWidgetItem(
                        str(j['subject']['title'])))
                    if j['room']['room_type'] == 'LAB':
                        self.mon_table.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лаб.работа'))
                    if j['room']['room_type'] == 'LEC':
                        self.mon_table.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лекция'))
                    if j['room']['room_type'] == 'SEM':
                        self.mon_table.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'семинар'))
                    self.mon_table.setItem(p, 2, QtGui.QTableWidgetItem(
                        j['teacher']['surname'] + ' ' +
                        j['teacher']['name'][0] + '.' +
                        j['teacher']['mid_name'][0] + '.'))
                if j['weekday'] == 'TUE':
                    p = j['position']
                    self.tue_table.setItem(p, 0, QtGui.QTableWidgetItem(
                        str(j['subject']['title'])))
                    if j['room']['room_type'] == 'LAB':
                        self.tue_table.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лаб.работа'))
                    if j['room']['room_type'] == 'LEC':
                        self.tue_table.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лекция'))
                    if j['room']['room_type'] == 'SEM':
                        self.tue_table.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'семинар'))
                    self.tue_table.setItem(p, 2, QtGui.QTableWidgetItem(
                        j['teacher']['surname'] + ' ' +
                        j['teacher']['name'][0] + '.' +
                        j['teacher']['mid_name'][0] + '.'))
                if j['weekday'] == 'WED':
                    p = j['position']
                    self.wed_table.setItem(p, 0, QtGui.QTableWidgetItem(
                        str(j['subject']['title'])))
                    if j['room']['room_type'] == 'LAB':
                        self.wed_table.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лаб.работа'))
                    if j['room']['room_type'] == 'LEC':
                        self.wed_table.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лекция'))
                    if j['room']['room_type'] == 'SEM':
                        self.wed_table.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'семинар'))
                    self.wed_table.setItem(p, 2, QtGui.QTableWidgetItem(
                        j['teacher']['surname'] + ' ' +
                        j['teacher']['name'][0] + '.' +
                        j['teacher']['mid_name'][0] + '.'))
                if j['weekday'] == 'THU':
                    p = j['position']
                    self.thu_table.setItem(p, 0, QtGui.QTableWidgetItem(
                        str(j['subject']['title'])))
                    if j['room']['room_type'] == 'LAB':
                        self.thu_table.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лаб.работа'))
                    if j['room']['room_type'] == 'LEC':
                        self.thu_table.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лекция'))
                    if j['room']['room_type'] == 'SEM':
                        self.thu_table.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'семинар'))
                    self.thu_table.setItem(p, 2, QtGui.QTableWidgetItem(
                        j['teacher']['surname'] + ' ' +
                        j['teacher']['name'][0] + '.' +
                        j['teacher']['mid_name'][0] + '.'))
                if j['weekday'] == 'FRI':
                    p = j['position']
                    self.fri_table.setItem(p, 0, QtGui.QTableWidgetItem(
                        str(j['subject']['title'])))
                    if j['room']['room_type'] == 'LAB':
                        self.fri_table.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лаб.работа'))
                    if j['room']['room_type'] == 'LEC':
                        self.fri_table.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лекция'))
                    if j['room']['room_type'] == 'SEM':
                        self.fri_table.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'семинар'))
                    self.fri_table.setItem(p, 2, QtGui.QTableWidgetItem(
                        j['teacher']['surname'] + ' ' +
                        j['teacher']['name'][0] + '.' +
                        j['teacher']['mid_name'][0] + '.'))
                if j['weekday'] == 'SAT':
                    p = j['position']
                    self.sat_table.setItem(p, 0, QtGui.QTableWidgetItem(
                        str(j['subject']['title'])))
                    if j['room']['room_type'] == 'LAB':
                        self.sat_table.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лаб.работа'))
                    if j['room']['room_type'] == 'LEC':
                        self.sat_table.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'лекция'))
                    if j['room']['room_type'] == 'SEM':
                        self.sat_table.setItem(p, 1, QtGui.QTableWidgetItem(
                            u'семинар'))
                    self.sat_table.setItem(p, 2, QtGui.QTableWidgetItem(
                        j['teacher']['surname'] + ' ' +
                        j['teacher']['name'][0] + '.' +
                        j['teacher']['mid_name'][0] + '.'))


def main():
    app = QtGui.QApplication(sys.argv)
    window = Application()
    window.show()
    app.exec_()


if __name__ == '__main__':
    main()
