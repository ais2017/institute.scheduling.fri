from scheduling.domain.branch import *
from scheduling.database.dbgateway import DBGateway


class DummyGateway(DBGateway):
    def __init__(self):
        branch1 = Branch(123, "Math is a Queen")
        # subjects
        branch1.add_subject(111, "Integral Calculus", 0, 1, 1)
        branch1.add_subject(222, "Ordinary Differential Equations ", 0, 1, 1)
        branch1.add_subject(333, "Mathematical Analysis", 0, 1, 1)
        branch1.add_subject(444, "Analytical Geometry", 0, 1, 1)
        branch1.add_subject(555, "Physics. Mechanics", 1, 1, 1)
        branch1.add_subject(666, "Physics. Electricity", 1, 1, 1)
        branch1.add_subject(777, "Physics. Atomic", 1, 1, 1)
        # teachers
        branch1.add_teacher(221, "Telyakovsky", "Dmitry", "Sergeevich", [111, 333])
        branch1.add_teacher(222, "Kamynin", "Vitaly", "Leonidovich", [222, 444])
        # plans
        branch1.add_plan(111, "1 year. Bachelor", [333, 444])
        # groups
        branch1.add_group(111, "B101", 111)
        branch1.add_group(222, "B102", 111)
        # rooms
        branch1.add_room(1, Activity.LAB)
        branch1.add_room(2, Activity.LAB)
        branch1.add_room(3, Activity.LEC)
        branch1.add_room(4, Activity.LEC)
        branch1.add_room(5, Activity.LEC)
        branch1.add_room(6, Activity.SEM)
        branch1.add_room(7, Activity.SEM)
        branch1.add_room(8, Activity.SEM)
        branch2 = Branch(456, "Unknown")
        self.branches = [branch1, branch2]

    def add_branch(self, branch_json):
        pass

    def add_room(self, branch_id, room_json):
        pass

    def add_subject(self, branch_id, subj_json):
        pass

    def add_teacher(self, branch_id, teacher_json):
        pass

    def add_plan(self, branch_id, plan_json):
        pass

    def add_group(self, branch_id, group_json):
        pass

    def save_schedule(self, branch_id, sched_json, confirmed):
        pass

    def delete_old_schedules(self, branch_id, confirmed):
        pass

    def load_database_info(self):
        return self.branches

    def delete_branch(self, branch_id):
        pass

    def delete_room(self, branch_id, room_num):
        pass

    def delete_subject(self, branch_id, subj_id):
        pass

    def delete_teacher(self, branch_id, teacher_id):
        pass

    def delete_plan(self, branch_id, plan_id):
        pass

    def delete_group(self, branch_id, group_id):
        pass
