
class DBGateway(object):
    def add_branch(self, branch_json):
        pass

    def add_room(self, branch_id, room_json):
        pass

    def add_subject(self, branch_id, subj_json):
        pass

    def add_teacher(self, branch_id, teacher_json):
        pass

    def add_plan(self, branch_id, plan_json):
        pass

    def add_group(self, branch_id, group_json):
        pass

    def save_schedule(self, branch_id, sched_json, confirmed):
        pass

    def delete_old_schedules(self, branch_id, confirmed):
        pass

    def load_database_info(self):
        pass

    def delete_branch(self, branch_id):
        pass

    def delete_room(self, branch_id, room_num):
        pass

    def delete_subject(self, branch_id, subj_id):
        pass

    def delete_teacher(self, branch_id, teacher_id):
        pass

    def delete_plan(self, branch_id, plan_id):
        pass

    def delete_group(self, branch_id, group_id):
        pass

