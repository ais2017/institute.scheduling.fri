from scheduling.database.dbgateway import DBGateway
from scheduling.domain.branch import *
import postgresql
import json


class PSQLGateway(DBGateway):
    def __init__(self, db):
        self.db = postgresql.open(db)

    def get_all_branches(self):
        branches = self.db.query("""
                                 SELECT * FROM branch;
                                 """)
        return branches

    def add_branch(self, branch_json):
        branch = json.loads(branch_json)
        self.db.query("""
                        INSERT INTO branch (
                            id,
                            title)
                        VALUES ($1, $2);
                        """, branch['id'], branch['title'])

    def get_all_rooms(self, branch_id):
        return self.db.query("""
                         SELECT room_num, room_type FROM room
                         WHERE branch_id=$1;
                         """, branch_id)

    def add_room(self, branch_id, room_json):
        activity_to_index = {
            'LAB': 0,
            'LEC': 1,
            'SEM': 2
        }
        room = json.loads(room_json)
        self.db.query("""
                        INSERT INTO room (
                            room_num,
                            branch_id,
                            room_type)
                        VALUES ($1, $2, $3);
                        """, room['room_num'], branch_id, activity_to_index[room['room_type']])

    def get_all_subjects(self, branch_id):
        return self.db.query("""
                         SELECT * FROM subject
                         WHERE branch_id=$1;
                         """, branch_id)

    def add_subject(self, branch_id, subj_json):
        subj = json.loads(subj_json)
        self.db.query("""
                        INSERT INTO subject (
                            id,
                            branch_id,
                            title,
                            lab_load,
                            lec_load,
                            sem_load)
                        VALUES ($1, $2, $3, $4, $5, $6);
                        """,
                      subj['id'], branch_id, subj['title'],
                      subj['lab_load'], subj['lec_load'], subj['sem_load'])

    def get_all_teachers(self, branch_id):
        return self.db.query("""
                             SELECT * FROM teacher
                             WHERE branch_id=$1;
                             """, branch_id)

    def add_teacher(self, branch_id, teacher_json):
        teacher = json.loads(teacher_json)
        self.db.query("""
                        INSERT INTO teacher (
                            id,
                            branch_id,
                            surname,
                            name,
                            mid_name)
                        VALUES ($1, $2, $3, $4, $5);
                        """,
                      teacher['id'], branch_id, teacher['surname'],
                      teacher['name'], teacher['mid_name'])
        for i in teacher['subjects']:
            self.db.query("""
                            INSERT INTO subject_for_teacher (
                                subject_id,
                                teacher_id)
                            VALUES ($1, $2);
                            """, i['id'], teacher['id'])

    def get_all_plans(self, branch_id):
        return self.db.query("""
                             SELECT * FROM plan
                             WHERE branch_id=$1;
                             """, branch_id)

    def add_plan(self, branch_id, plan_json):
        plan = json.loads(plan_json)
        self.db.query("""
                        INSERT INTO plan (
                            id,
                            branch_id,
                            title)
                        VALUES ($1, $2, $3);
                        """, plan['id'], branch_id, plan['title'])
        for i in plan['subjects']:
            self.db.query("""
                            INSERT INTO subject_in_plan (
                                subject_id,
                                plan_id)
                            VALUES ($1, $2);
                            """, i['id'], plan['id'])

    def get_all_groups(self, branch_id):
        return self.db.query("""
                             SELECT * FROM groups
                             WHERE branch_id=$1;
                             """, branch_id)

    def add_group(self, branch_id, group_json):
        group = json.loads(group_json)
        self.db.query("""
                        INSERT INTO groups (
                            id,
                            branch_id,
                            title,
                            plan_id)
                        VALUES ($1, $2, $3, $4);
                        """, group['id'], branch_id, group['title'], group['plan']['id'])

    def get_all_subjects_for_teacher(self, teacher_id):
        return self.db.query("""
                                 SELECT * FROM subject_for_teacher
                                 WHERE teacher_id=$1;
                                 """, teacher_id)

    def get_all_subjects_in_plan(self, plan_id):
        return self.db.query("""
                                 SELECT * FROM subject_in_plan
                                 WHERE plan_id=$1;
                                 """, plan_id)

    def save_schedule(self, branch_id, sched_json, confirmed):
        weekday_to_index = {
            'MON': 0,
            'TUE': 1,
            'WED': 2,
            'THU': 3,
            'FRI': 4,
            'SAT': 5
        }
        sched = json.loads(sched_json)
        id = sched['id']
        self.db.query("""
                        INSERT INTO schedule (
                            id,
                            branch_id,
                            confirmed)
                        VALUES ($1, $2, $3);
                        """, id, branch_id, confirmed)
        for i in sched['group_schedules']:
            for j in i['classes_list']:
                self.db.query("""
                                INSERT INTO study_class (
                                    schedule_id,
                                    branch_id,
                                    teacher_id,
                                    group_id,
                                    subject_id,
                                    room_room_num,
                                    weekday,
                                    position,
                                    confirmed)
                                VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9);
                                """,
                              id, branch_id,
                              j['teacher']['id'],
                              j['group']['id'],
                              j['subject']['id'],
                              j['room']['room_num'],
                              weekday_to_index[j['weekday']],
                              j['position'],
                              confirmed)

    def delete_old_schedules(self, branch_id, confirmed):
        self.db.query("""
                        DELETE FROM study_class
                        WHERE branch_id = $1 and confirmed = $2;
                        """, branch_id, confirmed)
        self.db.query("""
                        DELETE FROM schedule
                        WHERE branch_id = $1 and confirmed = $2;
                        """, branch_id, confirmed)

    def get_schedule(self, branch_id, confirmed):
        ret = self.db.query("""
                                 SELECT * FROM schedule
                                 WHERE branch_id=$1 and confirmed=$2;
                                 """, branch_id, confirmed)
        if len(ret) != 0:
            return ret[0]
        else:
            return None

    def get_schedule_classes(self, id, confirmed):
        return self.db.query("""
                                 SELECT * FROM study_class
                                 WHERE schedule_id=$1 and confirmed=$2;
                                 """, id, confirmed)

    # load all the info from the database during start
    def load_database_info(self):
        branches_list = []
        branches_db = self.get_all_branches()
        for i in range(len(branches_db)):
            branch_id = branches_db[i]['id']
            tmp_branch = Branch(branches_db[i]['id'], branches_db[i]['title'])
            rooms_db = self.get_all_rooms(branch_id)
            for j in range(len(rooms_db)):
                tmp_branch.add_room(rooms_db[j]['room_num'], Activity(rooms_db[j]['room_type']))
            subj_db = self.get_all_subjects(branch_id)
            for j in range(len(subj_db)):
                tmp_branch.add_subject(subj_db[j]['id'], subj_db[j]['title'],
                                       subj_db[j]['lab_load'], subj_db[j]['lec_load'],
                                       subj_db[j]['sem_load'])
            teachers_db = self.get_all_teachers(branch_id)
            for j in range(len(teachers_db)):
                t_subj_db = self.get_all_subjects_for_teacher(teachers_db[j]['id'])
                t_subj_ids = []
                for k in range(len(t_subj_db)):
                    t_subj_ids.append(t_subj_db[k]['subject_id'])
                tmp_branch.add_teacher(teachers_db[j]['id'],
                                       teachers_db[j]['surname'],
                                       teachers_db[j]['name'],
                                       teachers_db[j]['mid_name'],
                                       t_subj_ids)
            plans_db = self.get_all_plans(branch_id)
            for j in range(len(plans_db)):
                p_subj_db = self.get_all_subjects_in_plan(plans_db[j]['id'])
                p_subj_ids = []
                for k in range(len(p_subj_db)):
                    p_subj_ids.append(p_subj_db[k]['subject_id'])
                tmp_branch.add_plan(plans_db[j]['id'], plans_db[j]['title'], p_subj_ids)
            groups_db = self.get_all_groups(branch_id)
            for j in range(len(groups_db)):
                tmp_branch.add_group(groups_db[j]['id'],
                                     groups_db[j]['title'],
                                     groups_db[j]['plan_id'])
            draft_schedule_db = self.get_schedule(branch_id, 0)
            if draft_schedule_db is not None:
                draft_classes_db = self.get_schedule_classes(draft_schedule_db['id'], 0)
                draft_schedule = Schedule(draft_schedule_db['id'])
                for j in tmp_branch.group_list:
                    group_schedule = GroupSchedule(j)
                    for k in draft_classes_db:
                        if k['group_id'] == j.id:
                            for l in tmp_branch.subject_list:
                                if k['subject_id'] == l.id:
                                    for m in tmp_branch.teacher_list:
                                        if k['teacher_id'] == m.id:
                                            for n in tmp_branch.room_list:
                                                if k['room_room_num'] == n.room_num:
                                                    group_schedule.add_study_class(
                                                        StudyClass(Weekday(k['weekday']),
                                                                   k['position'], l, m, j, n))
                    draft_schedule.add_group_schedule(group_schedule)
                tmp_branch.draft_schedule = draft_schedule
            cur_schedule_db = self.get_schedule(branch_id, 1)
            if cur_schedule_db is not None:
                cur_classes_db = self.get_schedule_classes(cur_schedule_db['id'], 1)
                cur_schedule = Schedule(cur_schedule_db['id'])
                for j in tmp_branch.group_list:
                    group_schedule = GroupSchedule(j)
                    for k in cur_classes_db:
                        if k['group_id'] == j.id:
                            for l in tmp_branch.subject_list:
                                if k['subject_id'] == l.id:
                                    for m in tmp_branch.teacher_list:
                                        if k['teacher_id'] == m.id:
                                            for n in tmp_branch.room_list:
                                                if k['room_room_num'] == n.room_num:
                                                    group_schedule.add_study_class(
                                                        StudyClass(Weekday(k['weekday']),
                                                                   k['position'], l, m, j, n))
                    cur_schedule.add_group_schedule(group_schedule)
                tmp_branch.cur_schedule = cur_schedule
            branches_list.append(tmp_branch)
        return branches_list
