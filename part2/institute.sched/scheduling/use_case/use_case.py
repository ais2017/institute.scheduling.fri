# -*- coding: utf-8 -*-
from scheduling.domain.branch import *
from scheduling.database import *
from scheduling.serialize.serializers import CustomJsonEncoder
import json

branches_list = []


# ADDITIONAL FUNCTIONS
def branch_dup_check(title):
    for i in branches_list:
        if i.title == title:
            return True
    return False


def branch_exist(id):
    for i in branches_list:
        if i.id == id:
            return True
    return False


def room_exist(branch_id, room_num):
    for i in branches_list:
        if i.id == branch_id:
            for j in i.room_list:
                if j.room_num == room_num:
                    return True
    return False


def get_branch_by_id(branch_id):
    for i in range(len(branches_list)):
        if branches_list[i].id == branch_id:
            return i
    return None


def subject_exist(branch_id, title):
    for i in branches_list:
        if i.id == branch_id:
            for j in i.subject_list:
                if j.title == title:
                    return True
    return False


def plan_exist(branch_id, title):
    for i in branches_list:
        if i.id == branch_id:
            for j in i.plan_list:
                if j.title == title:
                    return True
    return False


def get_all_subj(branch_id):
    for i in branches_list:
        if i.id == branch_id:
            return i.subject_list


def group_exist(branch_id, title):
    for i in branches_list:
        if i.id == branch_id:
            for j in i.group_list:
                if j.title == title:
                    return True
    return False


# FUNCTIONS FOR UI
def add_branch(db, id, title):
    # check for duplicates
    ret = branch_dup_check(title)
    if ret is True:
        return -1, 'Ошибка: Филиал с таким названием уже существует!'
    branch = Branch(id, title)
    branches_list.append(branch)
    db.add_branch(json.dumps(branch, cls=CustomJsonEncoder, indent=2))
    return 0, 'Филиал успешно добавлен'


def get_all_branches():
    return 0, json.dumps(branches_list, cls=CustomJsonEncoder, indent=2)


def delete_branch(db, id):
    ret = branch_exist(id)
    if ret is False:
        return -1, 'Ошибка: Нет такого филиала!'
    for i in range(len(branches_list)):
        if branches_list[i].id == id:
            del branches_list[i]
            db.delete_branch(id)
            return 0, 'Филиал успешно удален'


def get_all_rooms(branch_id):
    for i in branches_list:
        if i.id == branch_id:
            return 0, json.dumps(i.room_list, cls=CustomJsonEncoder, indent=2)


def add_room(db, branch_id, room_num, room_type):
    ret = branch_exist(branch_id)
    if ret is False:
        return -1, 'Ошибка: Нет такого филиала!'
    ret = room_exist(branch_id, room_num)
    if ret is True:
        return -1, 'Ошибка: Аудитория с таким номером уже есть в данном филиале!'
    ind = get_branch_by_id(branch_id)
    branches_list[ind].add_room(room_num, Activity(room_type))
    db.add_room(branch_id, json.dumps(branches_list[ind].room_list[-1],
                                      cls=CustomJsonEncoder, indent=2))
    return 0, 'Аудитория успешно добавлена'


def delete_room(db, branch_id, room_num):
    ret = branch_exist(branch_id)
    if ret is False:
        return -1, 'Ошибка: Нет такого филиала!'
    for branch in branches_list:
        if branch.id == branch_id:
            for i in branch.room_list:
                if i.room_num == room_num:
                    branch.delete_room(room_num)
                    db.delete_room(branch_id, room_num)
                    return 0, 'Аудитория успешно удалена'
    return -1, 'Ошибка: Нет такой аудитории!'


def get_all_subjects(branch_id):
    for i in branches_list:
        if i.id == branch_id:
            return 0, json.dumps(i.subject_list, cls=CustomJsonEncoder, indent=2)


def add_subject(db, branch_id, id, title, lab_load, lec_load, sem_load):
    ret = branch_exist(branch_id)
    if ret is False:
        return -1, 'Ошибка: Нет такого филиала!'
    ret = subject_exist(branch_id, title)
    if ret is True:
        return -1, 'Ошибка! Такой предмет уже есть!'
    ind = get_branch_by_id(branch_id)
    branches_list[ind].add_subject(id, title, lab_load, lec_load, sem_load)
    db.add_subject(branch_id, json.dumps(branches_list[ind].subject_list[-1],
                                         cls=CustomJsonEncoder, indent=2))
    return 0, 'Предмет успешно добавлен'


def delete_subject(db, branch_id, id):
    ret = branch_exist(branch_id)
    if ret is False:
        return -1, 'Ошибка: Нет такого филиала!'
    for branch in branches_list:
        if branch.id == branch_id:
            for i in branch.subject_list:
                if i.id == id:
                    branch.delete_subject(id)
                    db.delete_subject(branch_id, id)
                    return 0, 'Предмет успешно удален'
    return -1, 'Ошибка: Нет такого предмета!'


def get_all_plans(branch_id):
    for i in branches_list:
        if i.id == branch_id:
            return 0, json.dumps(i.plan_list, cls=CustomJsonEncoder, indent=2)


def add_plan(db, branch_id, id, title, subj_titles):
    ret = branch_exist(branch_id)
    if ret is False:
        return -1, 'Ошибка: Нет такого филиала!'
    ret = plan_exist(branch_id, title)
    if ret is True:
        return -1, 'Ошибка: План с таким названием уже есть!'
    all_subjects = get_all_subj(branch_id)
    subjects = []
    for i in all_subjects:
        for j in subj_titles:
            if i.title == j:
                subjects.append(i.id)
    ind = get_branch_by_id(branch_id)
    branches_list[ind].add_plan(id, title, subjects)
    db.add_plan(branch_id, json.dumps(branches_list[ind].plan_list[-1],
                                      cls=CustomJsonEncoder, indent=2))
    return 0, 'План успешно добавлен'


def delete_plan(db, branch_id, id):
    ret = branch_exist(branch_id)
    if ret is False:
        return -1, 'Ошибка: Нет такого филиала!'
    for branch in branches_list:
        if branch.id == branch_id:
            for i in branch.plan_list:
                if i.id == id:
                    branch.delete_plan(id)
                    db.delete_plan(branch_id, id)
                    return 0, 'План успешно удален'
    return -1, 'Ошибка: Нет такого плана!'


def get_all_teachers(branch_id):
    for i in branches_list:
        if i.id == branch_id:
            return 0, json.dumps(i.teacher_list, cls=CustomJsonEncoder, indent=2)


def add_teacher(db, branch_id, id, surname, name, mid_name, subj_titles):
    ret = branch_exist(branch_id)
    if ret is False:
        return -1, 'Ошибка: Нет такого филиала!'
    all_subjects = get_all_subj(branch_id)
    subjects = []
    for i in all_subjects:
        for j in subj_titles:
            if i.title == j:
                subjects.append(i.id)
    ind = get_branch_by_id(branch_id)
    branches_list[ind].add_teacher(id, surname, name, mid_name, subjects)
    db.add_teacher(branch_id, json.dumps(branches_list[ind].teacher_list[-1],
                                         cls=CustomJsonEncoder, indent=2))
    return 0, 'Преподаватель успешно добавлен'


def delete_teacher(db, branch_id, id):
    ret = branch_exist(branch_id)
    if ret is False:
        return -1, 'Ошибка: Нет такого филиала!'
    for branch in branches_list:
        if branch.id == branch_id:
            for i in branch.teacher_list:
                if i.id == id:
                    branch.delete_teacher(id)
                    db.delete_teacher(branch_id, id)
                    return 0, 'Преподаватель успешно удален'
    return -1, 'Ошибка: Нет такого преподавателя!'


def get_all_groups(branch_id):
    for i in branches_list:
        if i.id == branch_id:
            return 0, json.dumps(i.group_list, cls=CustomJsonEncoder, indent=2)


def add_group(db, branch_id, id, title, plan_title):
    ret = branch_exist(branch_id)
    if ret is False:
        return -1, 'Ошибка: Нет такого филиала!'
    ret = group_exist(branch_id, title)
    if ret is True:
        return -1, 'Ошибка: Группа с таким названием уже существует в данном филиале!'
    ind = get_branch_by_id(branch_id)
    plan_id = None
    for i in branches_list[ind].plan_list:
        if i.title == plan_title:
            plan_id = i.id
    branches_list[ind].add_group(id, title, plan_id)
    db.add_group(branch_id, json.dumps(branches_list[ind].group_list[-1],
                                         cls=CustomJsonEncoder, indent=2))
    return 0, 'Группа успешно добавлена'


def delete_group(db, branch_id, id):
    ret = branch_exist(branch_id)
    if ret is False:
        return -1, 'Ошибка: Нет такого филиала!'
    for branch in branches_list:
        if branch.id == branch_id:
            for i in branch.group_list:
                if i.id == id:
                    branch.delete_group(id)
                    db.delete_group(branch_id, id)
                    return 0, 'Группа успешно удалена'
    return -1, 'Ошибка: Нет такой группы!'


# load actual state of the branches list
def load_current_info(db):
    branches_list[:] = db.load_database_info()
    return 0, json.dumps(branches_list, cls=CustomJsonEncoder, indent=2)


def create_schedule(db, branch_id):
    ret = branch_exist(branch_id)
    if ret is False:
        return -1, 'Ошибка: Нет такого филиала!'
    for i in range(len(branches_list)):
        if branches_list[i].id == branch_id:
            branches_list[i].create_schedule()
            return 0, json.dumps(branches_list[i].schedule_list,
                                 cls=CustomJsonEncoder, indent=2), 'Расписание успешно составлено'


def send_schedule(db, branch_id, idx):
    branch = None
    for i in range(len(branches_list)):
        if branches_list[i].id == branch_id:
            branch = branches_list[i]
    if branch is None:
        return -1, 'Ошибка: Нет такого филиала!'
    if len(branch.schedule_list) == 0:
        return -1, 'Ошибка: Сначала создайте расписание!'
    # clean database from previous draft schedules
    db.delete_old_schedules(branch_id, 0)
    branch.send_schedule(branch.schedule_list[idx].id)
    sched_json = json.dumps(branch.get_draft_schedule(), cls=CustomJsonEncoder, indent=2)
    db.save_schedule(branch_id, sched_json, 0)
    return 0, 'Расписание успешно отправлено на проверку'


def confirm_schedule(db, branch_id):
    branch = None
    for i in range(len(branches_list)):
        if branches_list[i].id == branch_id:
            branch = branches_list[i]
    if branch is None:
        return -1, 'Ошибка: Нет такого филиала!'
    if branch.draft_schedule is None:
        return -1, 'Ошибка: Нет расписаний, отправленных на проверку!'
    # clean database from previous confirmed schedules
    db.delete_old_schedules(branch_id, 1)
    branch.confirm_schedule()
    sched_json = json.dumps(branch.get_schedule(), cls=CustomJsonEncoder, indent=2)
    db.save_schedule(branch_id, sched_json, 1)
    return 0, 'Расписание успешно утверждено'
