import unittest
from scheduling.domain.schedule import *

# subjects
subj1 = Subject(111, "Integral Calculus", 0, 1, 1)
subj2 = Subject(222, "Ordinary Differential Equations ", 0, 1, 1)
subj3 = Subject(333, "Mathematical Analysis", 0, 1, 1)
subj4 = Subject(444, "Analytical Geometry", 0, 1, 1)
subj5 = Subject(555, "Physics. Mechanics", 1, 1, 1)
subj6 = Subject(666, "Physics. Electricity", 1, 1, 1)
subj7 = Subject(777, "Physics. Atomic", 1, 1, 1)
subj8 = Subject(888, "English. 1 year", 0, 0, 2)
subj9 = Subject(999, "English. 2 year", 0, 0, 1)
subj10 = Subject(1010, "Physical Education", 0, 0, 1)

# teachers
physics_teacher1 = Teacher(111, "Maslov", "Alexey", "Konstantinovich", [subj5, subj6, subj7])
physics_teacher2 = Teacher(112, "Oblizina", "Svetlana", "Vasilevna", [subj5, subj6])
math_teacher1 = Teacher(221, "Telyakovsky", "Dmitry", "Sergeevich", [subj1, subj3])
math_teacher2 = Teacher(222, "Kamynin", "Vitaly", "Leonidovich", [subj2, subj4])
english_teacher1 = Teacher(331, "Ivanova", "Elena", "Georgievna", [subj8, subj9])
english_teacher2 = Teacher(332, "Komochkina", "Elena", "Anatolevna", [subj8, subj9])
ph_educ_teacher = Teacher(441, "Nestrugina", "Victoria", "Vyacheslavovna", [subj10])
teachers = [physics_teacher1, physics_teacher2, math_teacher1, math_teacher2, english_teacher1, english_teacher2, ph_educ_teacher]

# plans
plan1 = Plan(111, "1 year. Bachelor", [subj3, subj4, subj5, subj8, subj10])
plan2 = Plan(222, "2 year. Bachelor", [subj1, subj2, subj6, subj9, subj10])

# groups
gr1_1 = Group(111, "B101", plan1)
gr1_2 = Group(112, "B102", plan1)
gr1_3 = Group(113, "B103", plan1)
gr2_1 = Group(221, "B201", plan2)
gr2_2 = Group(222, "B202", plan2)
gr2_3 = Group(223, "B203", plan2)

# rooms
room1 = Room(1, Activity.LAB)
room2 = Room(2, Activity.LAB)
room3 = Room(3, Activity.LEC)
room4 = Room(4, Activity.LEC)
room5 = Room(5, Activity.LEC)
room6 = Room(6, Activity.SEM)
room7 = Room(7, Activity.SEM)
room8 = Room(8, Activity.SEM)
room9 = Room(9, Activity.SEM)
room10 = Room(10, Activity.SEM)
rooms = [room1, room2, room3, room4, room5, room6, room7, room8, room9, room10]


class ScheduleTest(unittest.TestCase):
    def test_init(self):
        groups = [gr1_2, gr1_3, gr2_1]
        sched = Schedule(id, groups, teachers, rooms)
        # check if there enough schedules for all the groups
        self.assertEqual(len(sched.group_schedules), len(groups))
        classes_desired = 0
        classes_actual = 0
        for i in range(len(groups)):
            for j in range(len(groups[i].plan.subjects)):
                classes_desired += groups[i].plan.subjects[j].lab_load
                classes_desired += groups[i].plan.subjects[j].lec_load
                classes_desired += groups[i].plan.subjects[j].sem_load
        for i in range(len(sched.group_schedules)):
            for j in range(len(sched.group_schedules[i].classes_list)):
                classes_actual += 1
        # compare desired number of classes with actual number in the schedule
        self.assertEqual(classes_actual, classes_desired)

    def test_init_exception_time(self):
        subj11 = Subject(1111, "New subject1", 2, 2, 2)
        subj12 = Subject(1212, "New subject2", 2, 2, 2)
        subj13 = Subject(1313, "New subject3", 2, 2, 2)
        teacher = Teacher(441, "Unknown", "Unknown", "Unknown", [subj11, subj12, subj13])
        teachers.append(teacher)
        plan = Plan(777, "Plan for somebody", [subj1, subj2, subj3, subj4, subj5, subj6, subj7, subj8, subj9, subj10, subj11, subj12])
        group = Group(777, "B777", plan)
        with self.assertRaises(Exception) as context:
            sched = Schedule(id, [group], teachers, rooms)
        self.assertEqual('schedule: too many classes for the group!', str(context.exception))

    def test_get_group_schedule(self):
        groups = [gr1_1, gr1_2, gr1_3]
        sched = Schedule(id, groups, teachers, rooms)
        gr_sched = sched.get_gr_schedule(111)
        self.assertEqual(gr_sched.group, gr1_1)
        classes_desired = 0
        classes_actual = 0
        for i in range(len(gr1_1.plan.subjects)):
            classes_desired += gr1_1.plan.subjects[i].lab_load
            classes_desired += gr1_1.plan.subjects[i].lec_load
            classes_desired += gr1_1.plan.subjects[i].sem_load
        for i in range(len(gr_sched.classes_list)):
                classes_actual += 1
        # compare desired number of classes with actual number in the schedule
        self.assertEqual(classes_actual, classes_desired)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(ScheduleTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
