import unittest
from random import randint, choice


from scheduling.domain.studyclass import *

# define variables as a global (the same for all the following tests)
wday = Weekday.WED
pos = 2
subj1 = Subject(111, "Physics. Mechanics", 1, 1, 2)  # subject the same for group and teacher
subj2 = Subject(222, "Math. Differential computations", 0, 2, 2)
subj3 = Subject(333, "Sociology", 0, 1, 1)
subj_gr = [subj1, subj2, subj3]
plan = Plan(123, "1st semester, T", subj_gr)
grp = Group(123, "B17-777", plan)
subj4 = Subject(444, "Physics. Optics", 1, 1, 1)
subj5 = Subject(555, "Physics. Atomic", 1, 1, 1)
subj_teach = [subj1, subj4, subj5]
teach = Teacher(123, "Ivanov", "Ivan", "Ivanovich", subj_teach)  # teaches physics only
room = Room(100, Activity.LAB)


class StudyClassTest(unittest.TestCase):

    def test_init(self):  # constructor
        st_class = StudyClass(wday, pos, subj1, teach, grp, room)
        self.assertEqual(st_class.weekday, wday)
        self.assertEqual(st_class.position, pos)
        self.assertEqual(st_class.subject, subj1)
        self.assertEqual(st_class.teacher, teach)
        self.assertEqual(st_class.group, grp)
        self.assertEqual(st_class.room, room)

    def test_init_exception_weekday(self):
        with self.assertRaises(Exception) as context:
            st_class = StudyClass("friday", pos, subj1, teach, grp, room)
        self.assertEqual('weekday: invalid day of the week!', str(context.exception))

    def test_init_exception_position(self):
        r = choice([(-100, 0), (DAILY_CLASSES_LIMIT+1, 100)])
        with self.assertRaises(Exception) as context:
            st_class = StudyClass(wday, randint(*r), subj1, teach, grp, room)
        self.assertEqual('position: invalid class position!', str(context.exception))

    def test_init_exception_subject(self):
        with self.assertRaises(Exception) as context:
            st_class = StudyClass(wday, pos, "Math", teach, grp, room)
        self.assertEqual('subject: invalid subject!', str(context.exception))

    def test_init_exception_teacher(self):
        with self.assertRaises(Exception) as context:
            st_class = StudyClass(wday, pos, subj1, "Ivanov Ivan Ivanovich", grp, room)
        self.assertEqual('teacher: invalid teacher!', str(context.exception))

    def test_init_exception_teacher_subject_support(self):
        # subject that teacher doesn't teach
        with self.assertRaises(Exception) as context:
            st_class = StudyClass(wday, pos, subj2, teach, grp, room)
        self.assertEqual('teacher: subject is not supported!', str(context.exception))

    def test_init_exception_group(self):
        with self.assertRaises(Exception) as context:
            st_class = StudyClass(wday, pos, subj1, teach, "B17-111", room)
        self.assertEqual('group: invalid group!', str(context.exception))

    def test_init_exception_group_subject_support(self):
        # subject which wasn't in a group plan
        with self.assertRaises(Exception) as context:
            st_class = StudyClass(wday, pos, subj4, teach, grp, room)
        self.assertEqual('group: subject is not supported!', str(context.exception))

    def test_init_exception_room(self):
        with self.assertRaises(Exception) as context:
            st_class = StudyClass(wday, pos, subj1, teach, grp, "123")
        self.assertEqual('room: invalid room!', str(context.exception))


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(StudyClassTest)
    unittest.TextTestRunner(verbosity=2).run(suite)