import unittest

from scheduling.domain.plan import *


class PlanTest(unittest.TestCase):
    def test_init(self):  # constructor
        subj1 = Subject(111, "Physics. Mechanics", 1, 1, 1)
        subj2 = Subject(222, "Physics. Optics", 1, 1, 1)
        subj3 = Subject(333, "Physics. Atomic", 1, 1, 1)
        subjects = [subj1, subj2, subj3]
        plan = Plan(123, "Physics FOREVER", subjects)
        self.assertEqual(plan.id, 123)
        self.assertEqual(plan.title, "Physics FOREVER")
        self.assertEqual(plan.subjects, subjects)

    def test_init_exception_subject(self):
        subjects = "Physics. Mechanics"
        with self.assertRaises(Exception) as context:
            plan = Plan(123, "Mechanics only", subjects)
        self.assertEqual('subjects: not a list!', str(context.exception))

    def test_init_exception_subject_list(self):
        subjects = ["Physics. Mechanics", "Physics. Optics"]
        with self.assertRaises(Exception) as context:
            plan = Plan(123, "Physics again", subjects)
        self.assertEqual('subjects: not a subject list!', str(context.exception))


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(PlanTest)
    unittest.TextTestRunner(verbosity=2).run(suite)