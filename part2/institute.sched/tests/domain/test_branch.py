import unittest
from scheduling.domain.branch import *


class ScheduleTest(unittest.TestCase):
    def test_init(self):
        branch = Branch(123, "MEPHI Moscow")
        self.assertEqual(branch.id, 123)
        self.assertEqual(branch.title, "MEPHI Moscow")

    def test_add_room(self):
        branch = Branch(123, "MEPHI Moscow")
        branch.add_room(1, Activity.LAB)
        self.assertEqual(len(branch.room_list), 1)
        # check the last element in the list
        self.assertEqual(branch.room_list[-1].room_num, 1)
        self.assertEqual(branch.room_list[-1].room_type, Activity.LAB)

    def test_delete_room(self):
        branch = Branch(123, "MEPHI Moscow")
        branch.add_room(1, Activity.LAB)
        branch.add_room(3, Activity.LEC)
        branch.add_room(5, Activity.SEM)
        self.assertEqual(len(branch.room_list), 3)
        branch.delete_room(3)
        self.assertEqual(len(branch.room_list), 2)

    def test_add_subject(self):
        branch = Branch(123, "MEPHI Moscow")
        branch.add_subject(888, "English. 1 year", 0, 0, 2)
        self.assertEqual(len(branch.subject_list), 1)
        # check the last element in the list
        self.assertEqual(branch.subject_list[-1].id, 888)
        self.assertEqual(branch.subject_list[-1].title, "English. 1 year")
        self.assertEqual(branch.subject_list[-1].lab_load, 0)
        self.assertEqual(branch.subject_list[-1].lec_load, 0)
        self.assertEqual(branch.subject_list[-1].sem_load, 2)

    def test_delete_subject(self):
        branch = Branch(123, "MEPHI Moscow")
        branch.add_subject(555, "Physics. Mechanics", 1, 1, 1)
        branch.add_subject(888, "English. 1 year", 0, 0, 2)
        branch.add_subject(111, "Integral Calculus", 0, 1, 1)
        self.assertEqual(len(branch.subject_list), 3)
        branch.delete_subject(555)
        self.assertEqual(len(branch.subject_list), 2)

    def test_add_plan(self):
        branch = Branch(123, "MEPHI Moscow")
        branch.add_subject(555, "Physics. Mechanics", 1, 1, 1)
        branch.add_subject(888, "English. 1 year", 0, 0, 2)
        branch.add_subject(111, "Integral Calculus", 0, 1, 1)
        branch.add_plan(123, "1 year. Bachelor", [555, 111])
        self.assertEqual(len(branch.plan_list), 1)
        # check the last element in the list
        self.assertEqual(branch.plan_list[-1].id, 123)
        self.assertEqual(branch.plan_list[-1].title, "1 year. Bachelor")
        self.assertEqual(len(branch.plan_list[-1].subjects), 2)

    def test_delete_plan(self):
        branch = Branch(123, "MEPHI Moscow")
        branch.add_subject(555, "Physics. Mechanics", 1, 1, 1)
        branch.add_subject(888, "English. 1 year", 0, 0, 2)
        branch.add_subject(111, "Integral Calculus", 0, 1, 1)
        branch.add_plan(123, "1 year. Bachelor1", [555, 111])
        branch.add_plan(124, "1 year. Bachelor2", [888, 111])
        branch.add_plan(125, "1 year. Bachelor3", [888, 555])
        self.assertEqual(len(branch.plan_list), 3)
        # check the last element in the list
        branch.delete_plan(125)
        self.assertEqual(len(branch.plan_list), 2)

    def test_add_group(self):
        branch = Branch(123, "MEPHI Moscow")
        branch.add_subject(555, "Physics. Mechanics", 1, 1, 1)
        branch.add_subject(888, "English. 1 year", 0, 0, 2)
        branch.add_subject(111, "Integral Calculus", 0, 1, 1)
        branch.add_plan(123, "1 year. Bachelor1", [555, 111])
        branch.add_plan(124, "1 year. Bachelor2", [888, 111])
        branch.add_plan(125, "1 year. Bachelor3", [888, 555])
        branch.add_group(111, "B101", 124)
        self.assertEqual(len(branch.group_list), 1)
        # check the last element in the list
        self.assertEqual(branch.group_list[-1].id, 111)
        self.assertEqual(branch.group_list[-1].title, "B101")
        self.assertEqual(branch.group_list[-1].plan.id, 124)
        self.assertEqual(branch.group_list[-1].plan.title, "1 year. Bachelor2")

    def test_delete_group(self):
        branch = Branch(123, "MEPHI Moscow")
        branch.add_subject(888, "English. 1 year", 0, 0, 2)
        branch.add_subject(111, "Integral Calculus", 0, 1, 1)
        branch.add_plan(124, "1 year. Bachelor2", [888, 111])
        branch.add_group(111, "B101", 124)
        branch.add_group(222, "B102", 124)
        self.assertEqual(len(branch.group_list), 2)
        branch.delete_group(111)
        self.assertEqual(len(branch.group_list), 1)

    def test_add_teacher(self):
        branch = Branch(123, "MEPHI Moscow")
        branch.add_subject(555, "Physics. Mechanics", 1, 1, 1)
        branch.add_subject(666, "Physics. Electricity", 1, 1, 1)
        branch.add_subject(777, "Physics. Atomic", 1, 1, 1)
        branch.add_teacher(123, "Maslov", "Alexey", "Konstantinovich", [555, 666, 777])
        self.assertEqual(len(branch.teacher_list), 1)
        # check the last element in the list
        self.assertEqual(branch.teacher_list[-1].id, 123)
        self.assertEqual(branch.teacher_list[-1].name, "Alexey")
        self.assertEqual(branch.teacher_list[-1].surname, "Maslov")
        self.assertEqual(branch.teacher_list[-1].mid_name, "Konstantinovich")
        self.assertEqual(len(branch.teacher_list[-1].subjects), 3)

    def test_delete_teacher(self):
        branch = Branch(123, "MEPHI Moscow")
        branch.add_subject(888, "English. 1 year", 0, 0, 2)
        branch.add_teacher(123, "Ivanova", "Elena", "Georgievna", [888])
        self.assertEqual(len(branch.teacher_list), 1)
        # check the last element in the list
        branch.delete_teacher(123)
        self.assertEqual(len(branch.teacher_list), 0)

    def test_create_schedule(self):
        branch = Branch(123, "MEPHI Moscow")
        # subjects
        branch.add_subject(111, "Integral Calculus", 0, 1, 1)
        branch.add_subject(222, "Ordinary Differential Equations ", 0, 1, 1)
        branch.add_subject(333, "Mathematical Analysis", 0, 1, 1)
        branch.add_subject(444, "Analytical Geometry", 0, 1, 1)
        branch.add_subject(555, "Physics. Mechanics", 1, 1, 1)
        branch.add_subject(666, "Physics. Electricity", 1, 1, 1)
        branch.add_subject(777, "Physics. Atomic", 1, 1, 1)
        branch.add_subject(888, "English. 1 year", 0, 0, 2)
        branch.add_subject(999, "English. 2 year", 0, 0, 1)
        branch.add_subject(1010, "Physical Education", 0, 0, 1)
        # teachers
        branch.add_teacher(111, "Maslov", "Alexey", "Konstantinovich", [555, 666, 777])
        branch.add_teacher(112, "Oblizina", "Svetlana", "Vasilevna", [555, 666])
        branch.add_teacher(221, "Telyakovsky", "Dmitry", "Sergeevich", [111, 333])
        branch.add_teacher(222, "Kamynin", "Vitaly", "Leonidovich", [222, 444])
        branch.add_teacher(331, "Ivanova", "Elena", "Georgievna", [888, 999])
        branch.add_teacher(332, "Komochkina", "Elena", "Anatolevna", [888, 999])
        branch.add_teacher(441, "Nestrugina", "Victoria", "Vyacheslavovna", [1010])
        # plans
        branch.add_plan(111, "1 year. Bachelor", [333, 444, 555, 888, 1010])
        branch.add_plan(222, "2 year. Bachelor", [111, 222, 666, 999, 1010])
        # groups
        branch.add_group(111, "B101", 111)
        branch.add_group(221, "B201", 222)
        branch.add_group(222, "B202", 222)
        # rooms
        branch.add_room(1, Activity.LAB)
        branch.add_room(2, Activity.LAB)
        branch.add_room(3, Activity.LEC)
        branch.add_room(4, Activity.LEC)
        branch.add_room(5, Activity.LEC)
        branch.add_room(6, Activity.SEM)
        branch.add_room(7, Activity.SEM)
        branch.add_room(8, Activity.SEM)
        branch.add_room(9, Activity.SEM)
        branch.add_room(10, Activity.SEM)
        branch.create_schedule()
        self.assertEqual(len(branch.schedule_list), SCHEDULES_NUM)
        # check if there enough schedules for all the groups
        for m in range(SCHEDULES_NUM):
            self.assertEqual(len(branch.schedule_list[m].group_schedules), len(branch.group_list))
            classes_desired = 0
            classes_actual = 0
            for i in range(len(branch.group_list)):
                for j in range(len(branch.group_list[i].plan.subjects)):
                    classes_desired += branch.group_list[i].plan.subjects[j].lab_load
                    classes_desired += branch.group_list[i].plan.subjects[j].lec_load
                    classes_desired += branch.group_list[i].plan.subjects[j].sem_load
            for i in range(len(branch.schedule_list[m].group_schedules)):
                for j in range(len(branch.schedule_list[m].group_schedules[i].classes_list)):
                    classes_actual += 1
            # compare desired number of classes with actual number in the schedule
            self.assertEqual(classes_actual, classes_desired)

    def test_create_schedule_exception(self):
        branch = Branch(123, "MEPHI Moscow")
        with self.assertRaises(Exception) as context:
            branch.create_schedule()
        self.assertEqual('create_schedule: not enough info!', str(context.exception))

    def test_send_schedule(self):
        branch = Branch(123, "MEPHI Moscow")
        # subjects
        branch.add_subject(111, "Integral Calculus", 0, 1, 1)
        branch.add_subject(222, "Ordinary Differential Equations ", 0, 1, 1)
        branch.add_subject(333, "Mathematical Analysis", 0, 1, 1)
        branch.add_subject(444, "Analytical Geometry", 0, 1, 1)
        branch.add_subject(555, "Physics. Mechanics", 1, 1, 1)
        branch.add_subject(666, "Physics. Electricity", 1, 1, 1)
        branch.add_subject(777, "Physics. Atomic", 1, 1, 1)
        branch.add_subject(888, "English. 1 year", 0, 0, 2)
        branch.add_subject(999, "English. 2 year", 0, 0, 1)
        branch.add_subject(1010, "Physical Education", 0, 0, 1)
        # teachers
        branch.add_teacher(111, "Maslov", "Alexey", "Konstantinovich", [555, 666, 777])
        branch.add_teacher(112, "Oblizina", "Svetlana", "Vasilevna", [555, 666])
        branch.add_teacher(221, "Telyakovsky", "Dmitry", "Sergeevich", [111, 333])
        branch.add_teacher(222, "Kamynin", "Vitaly", "Leonidovich", [222, 444])
        branch.add_teacher(331, "Ivanova", "Elena", "Georgievna", [888, 999])
        branch.add_teacher(332, "Komochkina", "Elena", "Anatolevna", [888, 999])
        branch.add_teacher(441, "Nestrugina", "Victoria", "Vyacheslavovna", [1010])
        # plans
        branch.add_plan(111, "1 year. Bachelor", [333, 444, 555, 888, 1010])
        branch.add_plan(222, "2 year. Bachelor", [111, 222, 666, 999, 1010])
        # groups
        branch.add_group(111, "B101", 111)
        branch.add_group(112, "B102", 111)
        branch.add_group(222, "B202", 222)
        # rooms
        branch.add_room(1, Activity.LAB)
        branch.add_room(2, Activity.LAB)
        branch.add_room(3, Activity.LEC)
        branch.add_room(4, Activity.LEC)
        branch.add_room(5, Activity.LEC)
        branch.add_room(6, Activity.SEM)
        branch.add_room(7, Activity.SEM)
        branch.add_room(8, Activity.SEM)
        branch.add_room(9, Activity.SEM)
        branch.add_room(10, Activity.SEM)
        branch.create_schedule()
        branch.send_schedule(branch.schedule_list[2].id)
        self.assertEqual(branch.draft_schedule, branch.schedule_list[2])

    def test_confirm_schedule(self):
        branch = Branch(123, "MEPHI Moscow")
        # subjects
        branch.add_subject(111, "Integral Calculus", 0, 1, 1)
        branch.add_subject(222, "Ordinary Differential Equations ", 0, 1, 1)
        branch.add_subject(333, "Mathematical Analysis", 0, 1, 1)
        branch.add_subject(444, "Analytical Geometry", 0, 1, 1)
        branch.add_subject(555, "Physics. Mechanics", 1, 1, 1)
        branch.add_subject(666, "Physics. Electricity", 1, 1, 1)
        branch.add_subject(777, "Physics. Atomic", 1, 1, 1)
        branch.add_subject(888, "English. 1 year", 0, 0, 2)
        branch.add_subject(999, "English. 2 year", 0, 0, 1)
        branch.add_subject(1010, "Physical Education", 0, 0, 1)
        # teachers
        branch.add_teacher(111, "Maslov", "Alexey", "Konstantinovich", [555, 666, 777])
        branch.add_teacher(112, "Oblizina", "Svetlana", "Vasilevna", [555, 666])
        branch.add_teacher(221, "Telyakovsky", "Dmitry", "Sergeevich", [111, 333])
        branch.add_teacher(222, "Kamynin", "Vitaly", "Leonidovich", [222, 444])
        branch.add_teacher(331, "Ivanova", "Elena", "Georgievna", [888, 999])
        branch.add_teacher(332, "Komochkina", "Elena", "Anatolevna", [888, 999])
        branch.add_teacher(441, "Nestrugina", "Victoria", "Vyacheslavovna", [1010])
        # plans
        branch.add_plan(111, "1 year. Bachelor", [333, 444, 555, 888, 1010])
        branch.add_plan(222, "2 year. Bachelor", [111, 222, 666, 999, 1010])
        # groups
        branch.add_group(111, "B101", 111)
        branch.add_group(112, "B102", 111)
        branch.add_group(221, "B103", 111)
        # rooms
        branch.add_room(1, Activity.LAB)
        branch.add_room(2, Activity.LAB)
        branch.add_room(3, Activity.LEC)
        branch.add_room(4, Activity.LEC)
        branch.add_room(5, Activity.LEC)
        branch.add_room(6, Activity.SEM)
        branch.add_room(7, Activity.SEM)
        branch.add_room(8, Activity.SEM)
        branch.add_room(9, Activity.SEM)
        branch.add_room(10, Activity.SEM)
        branch.create_schedule()
        branch.send_schedule(branch.schedule_list[2].id)
        branch.confirm_schedule()
        self.assertEqual(branch.cur_schedule, branch.draft_schedule)

    def test_get_schedule(self):
        branch = Branch(123, "MEPHI Moscow")
        # subjects
        branch.add_subject(111, "Integral Calculus", 0, 1, 1)
        branch.add_subject(222, "Ordinary Differential Equations ", 0, 1, 1)
        branch.add_subject(333, "Mathematical Analysis", 0, 1, 1)
        branch.add_subject(444, "Analytical Geometry", 0, 1, 1)
        branch.add_subject(555, "Physics. Mechanics", 1, 1, 1)
        branch.add_subject(666, "Physics. Electricity", 1, 1, 1)
        branch.add_subject(777, "Physics. Atomic", 1, 1, 1)
        branch.add_subject(888, "English. 1 year", 0, 0, 2)
        branch.add_subject(999, "English. 2 year", 0, 0, 1)
        branch.add_subject(1010, "Physical Education", 0, 0, 1)
        # teachers
        branch.add_teacher(111, "Maslov", "Alexey", "Konstantinovich", [555, 666, 777])
        branch.add_teacher(112, "Oblizina", "Svetlana", "Vasilevna", [555, 666])
        branch.add_teacher(221, "Telyakovsky", "Dmitry", "Sergeevich", [111, 333])
        branch.add_teacher(222, "Kamynin", "Vitaly", "Leonidovich", [222, 444])
        branch.add_teacher(331, "Ivanova", "Elena", "Georgievna", [888, 999])
        branch.add_teacher(332, "Komochkina", "Elena", "Anatolevna", [888, 999])
        branch.add_teacher(441, "Nestrugina", "Victoria", "Vyacheslavovna", [1010])
        # plans
        branch.add_plan(111, "1 year. Bachelor", [333, 444, 555, 888, 1010])
        branch.add_plan(222, "2 year. Bachelor", [111, 222, 666, 999, 1010])
        # groups
        branch.add_group(111, "B101", 111)
        branch.add_group(112, "B102", 111)
        branch.add_group(221, "B103", 111)
        # rooms
        branch.add_room(1, Activity.LAB)
        branch.add_room(2, Activity.LAB)
        branch.add_room(3, Activity.LEC)
        branch.add_room(4, Activity.LEC)
        branch.add_room(5, Activity.LEC)
        branch.add_room(6, Activity.SEM)
        branch.add_room(7, Activity.SEM)
        branch.add_room(8, Activity.SEM)
        branch.add_room(9, Activity.SEM)
        branch.add_room(10, Activity.SEM)
        branch.create_schedule()
        branch.send_schedule(branch.schedule_list[2].id)
        branch.confirm_schedule()
        current = branch.get_schedule()
        self.assertEqual(branch.cur_schedule, current)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(ScheduleTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
