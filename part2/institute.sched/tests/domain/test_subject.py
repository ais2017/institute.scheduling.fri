import unittest

from scheduling.domain.subject import *


class SubjectTest(unittest.TestCase):
    def test_init(self):  # constructor
        subj = Subject(123, "Object Oriented Programming", 1, 2, 1)
        self.assertEqual(subj.id, 123)
        self.assertEqual(subj.title, "Object Oriented Programming")
        self.assertEqual(subj.lab_load, 1)
        self.assertEqual(subj.lec_load, 2)
        self.assertEqual(subj.sem_load, 1)

    def test_init_exception_lab_load(self):
        with self.assertRaises(Exception) as context:
            subj = Subject(123, "Structure Modelling", 3, 1, 1)
        self.assertEqual('lab_load: classes week limit exceeded!', str(context.exception))

    def test_init_exception_lec_load(self):
        with self.assertRaises(Exception) as context:
            subj = Subject(123, "Structure Modelling", 1, 5, 1)
        self.assertEqual('lec_load: classes week limit exceeded!', str(context.exception))

    def test_init_exception_sem_load(self):
        with self.assertRaises(Exception) as context:
            subj = Subject(123, "Structure Modelling", 1, 1, 4)
        self.assertEqual('sem_load: classes week limit exceeded!', str(context.exception))


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(SubjectTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
