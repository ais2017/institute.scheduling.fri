import unittest

from scheduling.domain.room import *


class RoomTest(unittest.TestCase):
    def test_init(self):
        room = Room(100, Activity.LAB)
        self.assertEqual(room.room_num, 100)
        self.assertEqual(room.room_type, Activity.LAB)

    def test_init_exception_room_type(self):
        with self.assertRaises(Exception) as context:
            room = Room(101, "lecture room")
        self.assertEqual('room_type: invalid room type!', str(context.exception))


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(RoomTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
