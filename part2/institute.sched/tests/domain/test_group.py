import unittest

from scheduling.domain.group import *


class GroupTest(unittest.TestCase):
    def test_init(self):  # constructor
        subj1 = Subject(111, "Physics. Mechanics", 1, 1, 2)
        subj2 = Subject(222, "Math. Differential computations", 0, 2, 2)
        subj3 = Subject(333, "Sociology", 0, 1, 1)
        subjects = [subj1, subj2, subj3]
        plan = Plan(123, "1st semester, T", subjects)
        group = Group(123, "B17-777", plan)
        self.assertEqual(group.id, 123)
        self.assertEqual(group.title, "B17-777")
        self.assertEqual(group.plan, plan)

    def test_init_exception_plan(self):
        plan = "Physics. Mechanics"
        with self.assertRaises(Exception) as context:
            group = Group(123, "B17-777", plan)
        self.assertEqual('plan: invalid plan!', str(context.exception))


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(GroupTest)
    unittest.TextTestRunner(verbosity=2).run(suite)