import unittest

from scheduling.domain.teacher import *
from random import randint, choice

subj1 = Subject(111, "Physics. Mechanics", 1, 1, 1)
subj2 = Subject(222, "Physics. Optics", 1, 1, 1)
subj3 = Subject(333, "Physics. Atomic", 1, 1, 1)


class TeacherTest(unittest.TestCase):
    def test_init(self):  # constructor
        subjects = [subj1, subj2, subj3]
        teach = Teacher(123, "Ivanov", "Ivan", "Ivanovich", subjects)
        self.assertEqual(teach.id, 123)
        self.assertEqual(teach.surname, "Ivanov")
        self.assertEqual(teach.name, "Ivan")
        self.assertEqual(teach.mid_name, "Ivanovich")
        self.assertEqual(teach.subjects, subjects)
        self.assertEqual(teach.full_load, TEACHER_LOAD_LIMIT)

    def test_init_exception_subject(self):
        subjects = "Physics. Mechanics"
        with self.assertRaises(Exception) as context:
            teach = Teacher(123, "Ivanov", "Ivan", "Ivanovich", subjects)
        self.assertEqual('subjects: not a list!', str(context.exception))

    def test_init_exception_subject_list(self):
        subjects = ["Physics. Mechanics", "Physics. Optics"]
        with self.assertRaises(Exception) as context:
            teach = Teacher(123, "Ivanov", "Ivan", "Ivanovich", subjects)
        self.assertEqual('subjects: not a subject list!', str(context.exception))

    def test_full_load(self):
        subjects = [subj1, subj2, subj3]
        teach = Teacher(123, "Ivanov", "Ivan", "Ivanovich", subjects)
        r = randint(1, TEACHER_LOAD_LIMIT)
        teach.full_load = r
        self.assertEqual(teach.full_load, r)

    def test_full_load_exception(self):
        subjects = [subj1, subj2, subj3]
        teach = Teacher(123, "Ivanov", "Ivan", "Ivanovich", subjects)
        r = choice([(-100, -1), (TEACHER_LOAD_LIMIT + 1, 100)])
        with self.assertRaises(Exception) as context:
            teach.full_load = randint(*r)
        self.assertEqual('full_load: week load limit error!', str(context.exception))


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TeacherTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
