# -*- coding: utf-8 -*-
from scheduling.use_case.use_case import *
from scheduling.database.dummy_gateway import DummyGateway
import unittest

db = DummyGateway()
load_current_info(db)


class UseCaseTest(unittest.TestCase):
    def test_add_branch(self):
        ret = add_branch(db, 777, "MEPHI Moscow")
        self.assertEqual(ret[0], 0)
        self.assertEqual(ret[1], 'Филиал успешно добавлен')

    def test_add_branch_error_duplicate(self):
        ret = add_branch(db, 123, "MEPHI Moscow")
        self.assertEqual(ret[0], -1)
        self.assertEqual(ret[1], 'Ошибка: Филиал с таким названием уже существует!')

    def test_get_all_branches(self):
        ret = get_all_branches()
        self.assertEqual(ret[0], 0)
        self.assertEqual(isinstance(json.loads(ret[1]), list), True)

    def test_delete_branch(self):
        ret = delete_branch(db, 456)
        self.assertEqual(ret[0], 0)
        self.assertEqual(ret[1], 'Филиал успешно удален')

    def test_delete_branch_error_nonexistent(self):
        ret = delete_branch(db, 555)
        self.assertEqual(ret[0], -1)
        self.assertEqual(ret[1], 'Ошибка: Нет такого филиала!')

    def test_add_room(self):
        # add Activity.SEM
        ret = add_room(db, 123, 9, 2)
        self.assertEqual(ret[0], 0)
        self.assertEqual(ret[1], 'Аудитория успешно добавлена')

    def test_get_all_rooms(self):
        ret = get_all_rooms(123)
        self.assertEqual(ret[0], 0)
        self.assertEqual(isinstance(json.loads(ret[1]), list), True)

    def test_delete_room(self):
        ret = delete_room(db, 123, 8)
        self.assertEqual(ret[0], 0)
        self.assertEqual(ret[1], 'Аудитория успешно удалена')

    def test_add_subject(self):
        ret = add_subject(db, 123, 1010, "Physical Education", 0, 0, 1)
        self.assertEqual(ret[0], 0)
        self.assertEqual(ret[1], 'Предмет успешно добавлен')

    def test_get_all_subjects(self):
        ret = get_all_subjects(123)
        self.assertEqual(ret[0], 0)
        self.assertEqual(isinstance(json.loads(ret[1]), list), True)

    def test_delete_subject(self):
        ret = delete_subject(db, 123, 222)
        self.assertEqual(ret[0], 0)
        self.assertEqual(ret[1], 'Предмет успешно удален')

    def test_add_plan(self):
        ret = add_plan(db, 123, 555, "Telyakovsky fans", ["Integral Calculus", "Mathematical Analysis"])
        self.assertEqual(ret[0], 0)
        self.assertEqual(ret[1], 'План успешно добавлен')

    def test_get_all_plans(self):
        ret = get_all_plans(123)
        self.assertEqual(ret[0], 0)
        self.assertEqual(isinstance(json.loads(ret[1]), list), True)

    def test_delete_plan(self):
        ret = delete_plan(db, 123, 111)
        self.assertEqual(ret[0], 0)
        self.assertEqual(ret[1], 'План успешно удален')

    def test_add_teacher(self):
        ret = add_teacher(db, 123, 111, "Maslov", "Alexey", "Konstantinovich", ["Physics. Mechanics",
                                                                                "Physics. Electricity",
                                                                                "Physics. Atomic"])
        self.assertEqual(ret[0], 0)
        self.assertEqual(ret[1], 'Преподаватель успешно добавлен')

    def test_get_all_teachers(self):
        ret = get_all_teachers(123)
        self.assertEqual(ret[0], 0)
        self.assertEqual(isinstance(json.loads(ret[1]), list), True)

    def test_delete_teacher(self):
        ret = delete_teacher(db, 123, 222)
        self.assertEqual(ret[0], 0)
        self.assertEqual(ret[1], 'Преподаватель успешно удален')

    def test_add_group(self):
        ret = add_group(db, 123, 333, "B103", "1 year. Bachelor")
        self.assertEqual(ret[0], 0)
        self.assertEqual(ret[1], 'Группа успешно добавлена')

    def test_get_all_groups(self):
        ret = get_all_groups(123)
        self.assertEqual(ret[0], 0)
        self.assertEqual(isinstance(json.loads(ret[1]), list), True)

    def test_delete_group(self):
        ret = delete_group(db, 123, 222)
        self.assertEqual(ret[0], 0)
        self.assertEqual(ret[1], 'Группа успешно удалена')

    def test1_create_schedule(self):
        ret = create_schedule(db, 123)
        self.assertEqual(ret[0], 0)
        self.assertEqual(isinstance(json.loads(ret[1]), list), True)
        self.assertEqual(ret[2], 'Расписание успешно составлено')

    def test2_send_schedule(self):
        create_schedule(db, 123)
        ret = send_schedule(db, 123, 2)
        self.assertEqual(ret[0], 0)
        self.assertEqual(ret[1], 'Расписание успешно отправлено на проверку')

    def test3_confirm_schedule(self):
        create_schedule(db, 123)
        send_schedule(db, 123, 2)
        ret = confirm_schedule(db, 123)
        self.assertEqual(ret[0], 0)
        self.assertEqual(ret[1], 'Расписание успешно утверждено')

    def test_load_current_info(self):
        ret = load_current_info(db)
        self.assertEqual(ret[0], 0)
        self.assertEqual(isinstance(json.loads(ret[1]), list), True)


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(UseCaseTest)
    unittest.TextTestRunner(verbosity=2).run(suite)
